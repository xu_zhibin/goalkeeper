package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface UserMapper extends Mapper<User> {

    long countByUsername(@Param("username") String username);

    long countByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    List<User> selectList(@Param("condition") PageTextCondition condition);

}
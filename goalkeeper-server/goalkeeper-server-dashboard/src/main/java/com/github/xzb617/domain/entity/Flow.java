package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_flow")
public class Flow {
    @Id
    private Integer id;

    /**
     * 限流名称
     */
    @Column(name = "flow_name")
    private String flowName;

    /**
     * 状态 1：启用 0：未启用
     */
    private Boolean status;

    private String remark;

    /**
     * 系统保护的qps
     */
    @Column(name = "sys_qps")
    private Integer sysQps;

    /**
     * 系统保护的cpu载荷，0-100
     */
    @Column(name = "sys_cpu")
    private Integer sysCpu;

    /**
     * 系统保护的运行内存占用率，0-100
     */
    @Column(name = "sys_memory")
    private Integer sysMemory;

    /**
     * 拒绝策略
     */
    @Column(name = "reject_strategy")
    private String rejectStrategy;

    /**
     * 拒绝策略的响应状态码，默认：429
     */
    @Column(name = "reject_response_status")
    private Integer rejectResponseStatus;

    /**
     * 拒绝策略的响应体内容，支持json，默认：Blocking by GoalKeeper
     */
    @Column(name = "reject_response_content")
    private String rejectResponseContent;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "update_user")
    private String updateUser;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取限流名称
     *
     * @return flow_name - 限流名称
     */
    public String getFlowName() {
        return flowName;
    }

    /**
     * 设置限流名称
     *
     * @param flowName 限流名称
     */
    public void setFlowName(String flowName) {
        this.flowName = flowName == null ? null : flowName.trim();
    }

    /**
     * 获取状态 1：启用 0：未启用
     *
     * @return status - 状态 1：启用 0：未启用
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置状态 1：启用 0：未启用
     *
     * @param status 状态 1：启用 0：未启用
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取系统保护的qps
     *
     * @return sys_qps - 系统保护的qps
     */
    public Integer getSysQps() {
        return sysQps;
    }

    /**
     * 设置系统保护的qps
     *
     * @param sysQps 系统保护的qps
     */
    public void setSysQps(Integer sysQps) {
        this.sysQps = sysQps;
    }

    /**
     * 获取系统保护的cpu载荷，0-100
     *
     * @return sys_cpu - 系统保护的cpu载荷，0-100
     */
    public Integer getSysCpu() {
        return sysCpu;
    }

    /**
     * 设置系统保护的cpu载荷，0-100
     *
     * @param sysCpu 系统保护的cpu载荷，0-100
     */
    public void setSysCpu(Integer sysCpu) {
        this.sysCpu = sysCpu;
    }

    /**
     * 获取系统保护的运行内存占用率，0-100
     *
     * @return sys_memory - 系统保护的运行内存占用率，0-100
     */
    public Integer getSysMemory() {
        return sysMemory;
    }

    /**
     * 设置系统保护的运行内存占用率，0-100
     *
     * @param sysMemory 系统保护的运行内存占用率，0-100
     */
    public void setSysMemory(Integer sysMemory) {
        this.sysMemory = sysMemory;
    }

    /**
     * 获取拒绝策略
     *
     * @return reject_strategy - 拒绝策略
     */
    public String getRejectStrategy() {
        return rejectStrategy;
    }

    /**
     * 设置拒绝策略
     *
     * @param rejectStrategy 拒绝策略
     */
    public void setRejectStrategy(String rejectStrategy) {
        this.rejectStrategy = rejectStrategy == null ? null : rejectStrategy.trim();
    }

    /**
     * 获取拒绝策略的响应状态码，默认：429
     *
     * @return reject_response_status - 拒绝策略的响应状态码，默认：429
     */
    public Integer getRejectResponseStatus() {
        return rejectResponseStatus;
    }

    /**
     * 设置拒绝策略的响应状态码，默认：429
     *
     * @param rejectResponseStatus 拒绝策略的响应状态码，默认：429
     */
    public void setRejectResponseStatus(Integer rejectResponseStatus) {
        this.rejectResponseStatus = rejectResponseStatus;
    }

    /**
     * 获取拒绝策略的响应体内容，支持json，默认：Blocking by GoalKeeper
     *
     * @return reject_response_content - 拒绝策略的响应体内容，支持json，默认：Blocking by GoalKeeper
     */
    public String getRejectResponseContent() {
        return rejectResponseContent;
    }

    /**
     * 设置拒绝策略的响应体内容，支持json，默认：Blocking by GoalKeeper
     *
     * @param rejectResponseContent 拒绝策略的响应体内容，支持json，默认：Blocking by GoalKeeper
     */
    public void setRejectResponseContent(String rejectResponseContent) {
        this.rejectResponseContent = rejectResponseContent == null ? null : rejectResponseContent.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_user
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_user
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }
}
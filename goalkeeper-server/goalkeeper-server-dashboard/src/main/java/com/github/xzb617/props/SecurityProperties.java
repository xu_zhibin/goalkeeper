package com.github.xzb617.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 安全相关的配置
 * <p>
 *     1.令牌
 *     2.用户密码
 * </p>
 */
@Component
@ConfigurationProperties(prefix = "goalkeeper.security")
public class SecurityProperties {

    /**
     * Token 在请求头中 key
     */
    private String tokenHeader = "Authorization";

    /**
     * Token 秘钥（AES)，要求必须配置
     */
    private String tokenSecret;

    /**
     * 客户端访问服务端的Key
     */
    private String clientAccessKey;

    /**
     * 客户端访问服务端的秘钥
     */
    private String clientAccessSecret;

    /**
     * 初始密码，默认：666666
     */
    private String userInitPassword = "666666";

    /**
     * 对明文密码加密的秘钥, 默认是：空字符串，配置该项可增加用户密码的安全系数
     */
    private String userPasswordEncryptedSecret = "";

    public String getTokenHeader() {
        return tokenHeader;
    }

    public void setTokenHeader(String tokenHeader) {
        this.tokenHeader = tokenHeader;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public String getUserInitPassword() {
        return userInitPassword;
    }

    public void setUserInitPassword(String userInitPassword) {
        this.userInitPassword = userInitPassword;
    }

    public String getUserPasswordEncryptedSecret() {
        return userPasswordEncryptedSecret;
    }

    public void setUserPasswordEncryptedSecret(String userPasswordEncryptedSecret) {
        this.userPasswordEncryptedSecret = userPasswordEncryptedSecret;
    }

    public String getClientAccessKey() {
        return clientAccessKey;
    }

    public void setClientAccessKey(String clientAccessKey) {
        this.clientAccessKey = clientAccessKey;
    }

    public String getClientAccessSecret() {
        return clientAccessSecret;
    }

    public void setClientAccessSecret(String clientAccessSecret) {
        this.clientAccessSecret = clientAccessSecret;
    }
}

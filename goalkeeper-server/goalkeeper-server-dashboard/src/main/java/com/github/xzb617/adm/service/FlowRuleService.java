package com.github.xzb617.adm.service;

import com.github.xzb617.domain.entity.FlowRule;

import java.util.List;

public interface FlowRuleService {
    List<FlowRule> getListByFlowId(Integer flowId);

    void update(Integer flowId, List<FlowRule> rules);

    void deleteByFlowId(Integer flowId);
}

package com.github.xzb617.valid.validator;

import com.github.xzb617.valid.anno.IsEmailAddress;
import com.github.xzb617.valid.regex.PatternPool;
import com.github.xzb617.valid.regex.RegexMatcher;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsEmailAddressValidator implements ConstraintValidator<IsEmailAddress, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s==null || "".equals(s)) {
            return true;
        }
        return RegexMatcher.isMatch(PatternPool.EMAIL, s);
    }

}

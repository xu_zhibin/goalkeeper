package com.github.xzb617.client.flow.props;

import java.util.HashMap;
import java.util.Map;

public class RulesProperties {

    private Boolean enabled = true;

    private String supportRule = "IP,URL";

    /**
     * 默认支持类型： IP,URI
     */
    private Map<String, Map<String, Integer>> limits = new HashMap<>();

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getSupportRule() {
        return supportRule;
    }

    public void setSupportRule(String supportRule) {
        this.supportRule = supportRule;
    }

    public Map<String, Map<String, Integer>> getLimits() {
        return limits;
    }

    public void setLimits(Map<String, Map<String, Integer>> limits) {
        this.limits = limits;
    }

}

package com.github.xzb617.logs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 操作日志注解
 * @author xzb617
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OptLogger {

    /**
     * 操作描述
     * @return
     */
    String desc();

    /**
     * 是否屏蔽参数
     * @return
     */
    boolean shieldArgs() default false;

}

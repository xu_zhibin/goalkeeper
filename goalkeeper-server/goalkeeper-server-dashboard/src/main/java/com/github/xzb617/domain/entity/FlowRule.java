package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_flow_rule")
public class FlowRule {
    @Id
    private Integer id;

    @Column(name = "flow_id")
    private Integer flowId;

    @Column(name = "rule_type")
    private String ruleType;

    @Column(name = "rule_key")
    private String ruleKey;

    @Column(name = "rule_qps")
    private Integer ruleQps;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "create_user")
    private String createUser;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return flow_id
     */
    public Integer getFlowId() {
        return flowId;
    }

    /**
     * @param flowId
     */
    public void setFlowId(Integer flowId) {
        this.flowId = flowId;
    }

    /**
     * @return rule_type
     */
    public String getRuleType() {
        return ruleType;
    }

    /**
     * @param ruleType
     */
    public void setRuleType(String ruleType) {
        this.ruleType = ruleType == null ? null : ruleType.trim();
    }

    /**
     * @return rule_key
     */
    public String getRuleKey() {
        return ruleKey;
    }

    /**
     * @param ruleKey
     */
    public void setRuleKey(String ruleKey) {
        this.ruleKey = ruleKey == null ? null : ruleKey.trim();
    }

    /**
     * @return rule_qps
     */
    public Integer getRuleQps() {
        return ruleQps;
    }

    /**
     * @param ruleQps
     */
    public void setRuleQps(Integer ruleQps) {
        this.ruleQps = ruleQps;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_user
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }
}
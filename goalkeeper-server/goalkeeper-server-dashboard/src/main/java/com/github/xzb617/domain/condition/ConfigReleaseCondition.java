package com.github.xzb617.domain.condition;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.common.TextCondition;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 发布历史查询条件
 * @author xzb617
 */
public class ConfigReleaseCondition extends PageTextCondition {

    private Integer configGroupId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date bgnTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    public Integer getConfigGroupId() {
        return configGroupId;
    }

    public void setConfigGroupId(Integer configGroupId) {
        this.configGroupId = configGroupId;
    }

    public Date getBgnTime() {
        return bgnTime;
    }

    public void setBgnTime(Date bgnTime) {
        this.bgnTime = bgnTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}

package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.FlowStatusMapper;
import com.github.xzb617.adm.service.FlowStatusService;
import com.github.xzb617.domain.entity.FlowStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;

@Service
public class FlowStatusServiceImpl implements FlowStatusService {

    private final FlowStatusMapper flowStatusMapper;

    public FlowStatusServiceImpl(FlowStatusMapper flowStatusMapper) {
        this.flowStatusMapper = flowStatusMapper;
    }

    @Override
    public FlowStatus getById(String id) {
        return this.flowStatusMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional
    public void saveOrUpdateStatus(String flowName) {
        // 根据名称查询
        Example example = new Example(FlowStatus.class);
        example.createCriteria().andEqualTo("flowName", flowName);
        FlowStatus flowStatus = this.flowStatusMapper.selectOneByExample(example);
        if (flowStatus == null) {
            // 新增一条记录
            flowStatus = new FlowStatus();
            flowStatus.setFlowName(flowName);
            flowStatus.setChangedTime(new Date());
            this.flowStatusMapper.insertSelective(flowStatus);
        } else {
            flowStatus.setChangedTime(new Date());
            this.flowStatusMapper.updateByPrimaryKeySelective(flowStatus);
        }
    }
}

package com.github.xzb617.logs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.xzb617.security.SubjectHolder;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

@Aspect
public class OptLoggerAspectj {

    private final OptLoggerStorage optLoggerStorage;
    private final ObjectMapper objectMapper;

    public OptLoggerAspectj(OptLoggerStorage optLoggerStorage, ObjectMapper objectMapper) {
        this.optLoggerStorage = optLoggerStorage;
        this.objectMapper = objectMapper;
    }


    /**
     * 切入点，针对有 Logger 注解的方法进行切入
     */
    @Pointcut("@annotation(com.github.xzb617.logs.OptLogger)")
    public void pointCut(){}


    @Around("pointCut()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        // 获取注解
        OptLogger logger = this.getAnnotation(pjp);
        if (logger == null) {
            return pjp.proceed();
        }
        // 相关参数
        String optUser = SubjectHolder.getContext().getUsername();
        String optDesc = logger.desc();
        String reqURI = this.getURI();
        Signature signature = pjp.getSignature();
        String optMethod = signature.toString().split(" ")[1];
        String optArgs = null;
        if (!logger.shieldArgs()) {
            Object[] args = pjp.getArgs();
            optArgs = this.objectMapper.writeValueAsString(args);
            if (optArgs!=null && args.length==1) {
                // 当参数只有一个时，去掉首尾的中括号
                optArgs = optArgs.substring(1, optArgs.length()-1);
            }
        } else {
            optArgs = "******";
        }
        Date optTime = new Date();
        // 执行程序
        Object result = pjp.proceed();
        // 处理日志信息
        this.optLoggerStorage.saveLogRecord(reqURI, optMethod, optDesc, optArgs, optUser, optTime);
        // 返回解析后的结果
        return result;
    }

    /**
     * 获取注解
     * @param point
     * @return
     */
    private OptLogger getAnnotation(JoinPoint point) {
        Signature signature = point.getSignature();
        if (signature instanceof MethodSignature) {
            MethodSignature methodSignature = (MethodSignature) signature;
            Method method = methodSignature.getMethod();
            return method!=null ? method.getAnnotation(OptLogger.class) : null;
        }
        return null;
    }

    /**
     * 获取API的URI地址
     * @return
     */
    private String getURI() {
        HttpServletRequest request = this.getRequest();
        return request!=null ? request.getRequestURI():"";
    }

    /**
     * 获取 HttpServletRequest
     * @return
     */
    private HttpServletRequest getRequest() {
        ServletRequestAttributes attributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        return attributes!=null ? attributes.getRequest() : null;
    }

}

package com.github.xzb617.utils;

import com.github.xzb617.common.util.StrUtil;

/**
 * 对Map中的Key进行格式化
 * @author xzb617
 */
public class MapKeyFormatUtil {

    /**
     * 对含有 '.'、'/'的值进行转义
     * <p>
     *     比如： IP类型数值、URL类型数值均需要转义
     * </p>
     * @param key Map.key
     * @return String
     */
    public static String escape(String key) {
        if (StrUtil.isEmpty(key)) {
            return key;
        }
        // 含有 '.'
        if (key.contains(".") || key.contains("/")) {
            return "[" + key + "]";
        }
        return key;
    }


}

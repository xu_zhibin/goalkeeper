package com.github.xzb617.domain.vo.echarts;

import java.util.HashSet;
import java.util.Set;

/**
 * 图表的分类项
 * @author xzb617
 */
public class Legend {

    private Set<String> data = new HashSet<>();

    public Legend() {
    }

    public Legend(Set<String> data) {
        this.data = data;
    }

    public Set<String> getData() {
        return data;
    }

    public void setData(Set<String> data) {
        this.data = data;
    }
}

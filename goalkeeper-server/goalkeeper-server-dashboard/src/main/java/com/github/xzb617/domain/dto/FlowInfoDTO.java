package com.github.xzb617.domain.dto;

import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 创建或编辑Flow基础信息数据模型
 * @author xzb617
 */
public class FlowInfoDTO {

    @NotNull(message = "用户编号不能为空", groups = {Update.class})
    private Integer id;

    @NotBlank(message = "限流规则名不能为空", groups = {Insert.class})
    @Length(max = 50, message = "限流规则名长度不允许超过30个字", groups = {Insert.class})
    private String flowName;

    @NotNull(message = "状态不能为空", groups = {Insert.class, Update.class})
    private Boolean status;

    @Length(max = 100, message = "备注长度不允许超过100个字", groups = {Insert.class, Update.class})
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

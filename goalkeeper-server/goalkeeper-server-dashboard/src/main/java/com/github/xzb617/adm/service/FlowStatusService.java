package com.github.xzb617.adm.service;

import com.github.xzb617.domain.entity.FlowStatus;

public interface FlowStatusService {

    FlowStatus getById(String id);

    void saveOrUpdateStatus(String flowName);
}

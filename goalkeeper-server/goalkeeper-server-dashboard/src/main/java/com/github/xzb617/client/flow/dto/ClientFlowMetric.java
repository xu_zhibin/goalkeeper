package com.github.xzb617.client.flow.dto;

/**
 * 限流指标, 时间区间为约定为1分钟
 * @author xzb617
 */
public class ClientFlowMetric {

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 实例ID
     */
    private String instanceId;

    /**
     * 总访问量
     */
    private Long totalCount;

    /**
     * 成功通过
     */
    private Long passCount;

    /**
     * 被限流
     */
    private Long failCount;

    /**
     * 切片开始时间
     */
    private long startTime;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Long getPassCount() {
        return passCount;
    }

    public void setPassCount(Long passCount) {
        this.passCount = passCount;
    }

    public Long getFailCount() {
        return failCount;
    }

    public void setFailCount(Long failCount) {
        this.failCount = failCount;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @Override
    public String toString() {
        return "FlowMetric{" +
                "appId='" + appId + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", totalCount=" + totalCount +
                ", passCount=" + passCount +
                ", failCount=" + failCount +
                ", startTime=" + startTime +
                '}';
    }
}

package com.github.xzb617.adm.service;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.RegCenterConfigDTO;
import com.github.xzb617.domain.entity.RegCenterConfig;

import java.util.List;

public interface RegCenterConfigService {

    RegCenterConfig getCurrent();

    RegCenterConfig getByName(String regCenterName);

    List<RegCenterConfig> getList(PageTextCondition condition);

    void save(RegCenterConfigDTO dto);

    void update(RegCenterConfigDTO dto);

    void modifyStatus(RegCenterConfigDTO dto);

    void deleteById(Integer id);

}

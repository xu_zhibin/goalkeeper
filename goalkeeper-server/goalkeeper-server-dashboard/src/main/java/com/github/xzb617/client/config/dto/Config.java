package com.github.xzb617.client.config.dto;

public class Config {

    private String configSign;

    private Long configReleaseTime = 0L;

    private Long flowChangedTime = 0L;

    private String flowSign;

    private String routeSign;

    private Long routeChangedTime = 0L;

    public String getConfigSign() {
        return configSign;
    }

    public void setConfigSign(String configSign) {
        this.configSign = configSign;
    }

    public Long getConfigReleaseTime() {
        return configReleaseTime;
    }

    public void setConfigReleaseTime(Long configReleaseTime) {
        this.configReleaseTime = configReleaseTime;
    }

    public Long getFlowChangedTime() {
        return flowChangedTime;
    }

    public void setFlowChangedTime(Long flowChangedTime) {
        this.flowChangedTime = flowChangedTime;
    }

    public Long getRouteChangedTime() {
        return routeChangedTime;
    }

    public void setRouteChangedTime(Long routeChangedTime) {
        this.routeChangedTime = routeChangedTime;
    }

    public String getFlowSign() {
        return flowSign;
    }

    public void setFlowSign(String flowSign) {
        this.flowSign = flowSign;
    }

    public String getRouteSign() {
        return routeSign;
    }

    public void setRouteSign(String routeSign) {
        this.routeSign = routeSign;
    }
}

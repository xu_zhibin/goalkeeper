package com.github.xzb617.adm.service;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.RouteDTO;
import com.github.xzb617.domain.entity.RouteRule;

import java.util.List;
import java.util.Map;

public interface RouteRuleService {

    Map<String, Object> getClientFlowConfig(String flowName);

    RouteRule getById(Integer id);

    RouteRule getByName(String routeName);

    List<RouteRule> getList(PageTextCondition condition);

    void update(RouteDTO dto);

}

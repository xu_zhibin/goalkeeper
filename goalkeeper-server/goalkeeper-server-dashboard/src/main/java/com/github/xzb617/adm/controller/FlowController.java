package com.github.xzb617.adm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xzb617.adm.service.FlowParamService;
import com.github.xzb617.adm.service.FlowRuleService;
import com.github.xzb617.adm.service.FlowService;
import com.github.xzb617.domain.common.AjaxResponse;
import com.github.xzb617.domain.common.Page;
import com.github.xzb617.domain.common.PageData;
import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.FlowDTO;
import com.github.xzb617.domain.dto.FlowInfoDTO;
import com.github.xzb617.domain.entity.Flow;
import com.github.xzb617.domain.entity.FlowParam;
import com.github.xzb617.domain.entity.FlowRule;
import com.github.xzb617.domain.vo.FlowVO;
import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@RestController
@RequestMapping("/flow")
public class FlowController {

    private final FlowService flowService;
    private final FlowRuleService flowRuleService;
    private final FlowParamService flowParamService;

    public FlowController(FlowService flowService, FlowRuleService flowRuleService, FlowParamService flowParamService) {
        this.flowService = flowService;
        this.flowRuleService = flowRuleService;
        this.flowParamService = flowParamService;
    }

    @GetMapping("getById")
    public AjaxResponse getById(Integer id) {
        Flow flow = this.flowService.getById(id);
        List<FlowRule> rules = this.flowRuleService.getListByFlowId(id);
        List<FlowParam> params = this.flowParamService.getListByFlowId(id);
        return AjaxResponse.success().message("查询成功").data(new FlowVO(flow, rules, params));
    }

    @GetMapping("/getList")
    public AjaxResponse getList(PageTextCondition condition) {
        PageHelper.startPage(condition.getPageNum(), condition.getPageSize());
        List<Flow> details = this.flowService.getList(condition);
        PageInfo<Flow> pageInfo = new PageInfo<>(details);
        PageData<Flow> pageData = Page.toData(pageInfo);
        return AjaxResponse.success().message("查询成功").data(pageData);
    }


    @PostMapping("/saveInfo")
    public AjaxResponse saveInfo(@RequestBody @Validated(Insert.class) FlowInfoDTO dto) {
        this.flowService.saveInfo(dto);
        return AjaxResponse.success().message("创建成功");
    }

    @PutMapping("/updateInfo")
    public AjaxResponse updateInfo(@RequestBody @Validated(Update.class) FlowInfoDTO dto) {
        this.flowService.updateInfo(dto);
        return AjaxResponse.success().message("编辑成功");
    }

    @PutMapping("/update")
    public AjaxResponse update(@RequestBody FlowDTO dto) {
        this.flowService.update(dto);
        return AjaxResponse.success().message("更新成功");
    }

    @DeleteMapping("/deleteById")
    public AjaxResponse deleteById(@NotNull(message = "限流规则编号不能为空") Integer id) {
        this.flowService.deleteById(id);
        return AjaxResponse.success().message("删除成功");
    }
}

package com.github.xzb617.client.route.props;

import com.github.xzb617.client.route.dto.RouteRule;

import java.util.HashMap;
import java.util.Map;

public class RuleProperties {

    private Map<String, Map<String, RouteRule>> rules = new HashMap<>();

    public Map<String, Map<String, RouteRule>> getRules() {
        return rules;
    }

    public void setRules(Map<String, Map<String, RouteRule>> rules) {
        this.rules = rules;
    }

    @Override
    public String toString() {
        return "RuleProperties{" +
                "rules=" + rules.toString() +
                '}';
    }
}

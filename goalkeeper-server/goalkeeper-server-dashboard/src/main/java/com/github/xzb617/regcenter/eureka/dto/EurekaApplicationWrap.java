package com.github.xzb617.regcenter.eureka.dto;

public class EurekaApplicationWrap {

    private EurekaApplication application;

    public EurekaApplication getApplication() {
        return application;
    }

    public void setApplication(EurekaApplication application) {
        this.application = application;
    }
}

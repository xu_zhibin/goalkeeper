package com.github.xzb617.regcenter.eureka.dto;

public class EurekaApplicationsWrap {

    private EurekaApplications applications;

    public EurekaApplications getApplications() {
        return applications;
    }

    public void setApplications(EurekaApplications applications) {
        this.applications = applications;
    }
}

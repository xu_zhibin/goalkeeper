package com.github.xzb617.adm.service;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.ModifySelfPwdDTO;
import com.github.xzb617.domain.dto.UserDTO;
import com.github.xzb617.domain.entity.User;

import java.util.List;

public interface UserService {

    User getById(Integer id);

    User getByName(String username);

    List<User> getList(PageTextCondition condition);

    void save(UserDTO dto);

    void update(UserDTO dto);

    void deleteById(Integer id);

    void resetPwd(Integer id);

    void modifySelfPwd(ModifySelfPwdDTO dto);

}

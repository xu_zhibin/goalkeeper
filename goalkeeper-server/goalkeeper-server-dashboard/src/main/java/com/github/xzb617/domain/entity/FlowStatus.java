package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_flow_status")
public class FlowStatus {
    @Id
    @Column(name = "flow_name")
    private String flowName;

    /**
     * 配置更新时间
     */
    @Column(name = "changed_time")
    private Date changedTime;

    /**
     * @return flow_name
     */
    public String getFlowName() {
        return flowName;
    }

    /**
     * @param flowName
     */
    public void setFlowName(String flowName) {
        this.flowName = flowName == null ? null : flowName.trim();
    }

    /**
     * 获取配置更新时间
     *
     * @return changed_time - 配置更新时间
     */
    public Date getChangedTime() {
        return changedTime;
    }

    /**
     * 设置配置更新时间
     *
     * @param changedTime 配置更新时间
     */
    public void setChangedTime(Date changedTime) {
        this.changedTime = changedTime;
    }
}
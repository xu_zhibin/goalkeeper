package com.github.xzb617.domain.entity;

import javax.persistence.*;

@Table(name = "gk_route_rule_item")
public class RouteRuleItem {
    @Id
    private Integer id;

    @Column(name = "route_rule_id")
    private Integer routeRuleId;

    @Column(name = "rule_type")
    private String ruleType;

    @Column(name = "rule_key")
    private String ruleKey;

    @Column(name = "caller_app_id")
    private String callerAppId;

    @Column(name = "caller_key")
    private String callerKey;

    @Column(name = "caller_value")
    private String callerValue;

    @Column(name = "callee_app_id")
    private String calleeAppId;

    @Column(name = "callee_label")
    private String calleeLabel;

    @Column(name = "callee_label_value")
    private String calleeLabelValue;

    /**
     * 权重，1-100
     */
    private Integer weight;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return route_rule_id
     */
    public Integer getRouteRuleId() {
        return routeRuleId;
    }

    /**
     * @param routeRuleId
     */
    public void setRouteRuleId(Integer routeRuleId) {
        this.routeRuleId = routeRuleId;
    }

    /**
     * @return rule_type
     */
    public String getRuleType() {
        return ruleType;
    }

    /**
     * @param ruleType
     */
    public void setRuleType(String ruleType) {
        this.ruleType = ruleType == null ? null : ruleType.trim();
    }

    /**
     * @return rule_key
     */
    public String getRuleKey() {
        return ruleKey;
    }

    /**
     * @param ruleKey
     */
    public void setRuleKey(String ruleKey) {
        this.ruleKey = ruleKey == null ? null : ruleKey.trim();
    }

    /**
     * @return caller_app_id
     */
    public String getCallerAppId() {
        return callerAppId;
    }

    /**
     * @param callerAppId
     */
    public void setCallerAppId(String callerAppId) {
        this.callerAppId = callerAppId == null ? null : callerAppId.trim();
    }

    /**
     * @return caller_key
     */
    public String getCallerKey() {
        return callerKey;
    }

    /**
     * @param callerKey
     */
    public void setCallerKey(String callerKey) {
        this.callerKey = callerKey == null ? null : callerKey.trim();
    }

    /**
     * @return caller_value
     */
    public String getCallerValue() {
        return callerValue;
    }

    /**
     * @param callerValue
     */
    public void setCallerValue(String callerValue) {
        this.callerValue = callerValue == null ? null : callerValue.trim();
    }

    /**
     * @return callee_app_id
     */
    public String getCalleeAppId() {
        return calleeAppId;
    }

    /**
     * @param calleeAppId
     */
    public void setCalleeAppId(String calleeAppId) {
        this.calleeAppId = calleeAppId == null ? null : calleeAppId.trim();
    }

    /**
     * @return callee_label
     */
    public String getCalleeLabel() {
        return calleeLabel;
    }

    /**
     * @param calleeLabel
     */
    public void setCalleeLabel(String calleeLabel) {
        this.calleeLabel = calleeLabel == null ? null : calleeLabel.trim();
    }

    /**
     * @return callee_label_value
     */
    public String getCalleeLabelValue() {
        return calleeLabelValue;
    }

    /**
     * @param calleeLabelValue
     */
    public void setCalleeLabelValue(String calleeLabelValue) {
        this.calleeLabelValue = calleeLabelValue == null ? null : calleeLabelValue.trim();
    }

    /**
     * 获取权重，1-100
     *
     * @return weight - 权重，1-100
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * 设置权重，1-100
     *
     * @param weight 权重，1-100
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
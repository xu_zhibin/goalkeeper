package com.github.xzb617.adm.service;

import com.github.xzb617.domain.common.TextCondition;
import com.github.xzb617.domain.vo.LineChartVO;

import java.util.List;

public interface RouteFlowMetricService {

    List<LineChartVO> getClientRouteMetrics(TextCondition condition);

}

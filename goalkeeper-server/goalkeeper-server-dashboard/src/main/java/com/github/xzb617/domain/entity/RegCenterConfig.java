package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_reg_center_config")
public class RegCenterConfig {
    @Id
    private Integer id;

    @Column(name = "reg_center_name")
    private String regCenterName;

    /**
     * 注册中心类型，目前仅支持EUREKA
     */
    @Column(name = "reg_center_type")
    private String regCenterType;

    /**
     * 服务地址
     */
    @Column(name = "reg_center_addr")
    private String regCenterAddr;

    /**
     * 是否
     */
    private Boolean status;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "update_user")
    private String updateUser;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return reg_center_name
     */
    public String getRegCenterName() {
        return regCenterName;
    }

    /**
     * @param regCenterName
     */
    public void setRegCenterName(String regCenterName) {
        this.regCenterName = regCenterName == null ? null : regCenterName.trim();
    }

    /**
     * 获取注册中心类型，目前仅支持EUREKA
     *
     * @return reg_center_type - 注册中心类型，目前仅支持EUREKA
     */
    public String getRegCenterType() {
        return regCenterType;
    }

    /**
     * 设置注册中心类型，目前仅支持EUREKA
     *
     * @param regCenterType 注册中心类型，目前仅支持EUREKA
     */
    public void setRegCenterType(String regCenterType) {
        this.regCenterType = regCenterType == null ? null : regCenterType.trim();
    }

    /**
     * 获取服务地址
     *
     * @return reg_center_addr - 服务地址
     */
    public String getRegCenterAddr() {
        return regCenterAddr;
    }

    /**
     * 设置服务地址
     *
     * @param regCenterAddr 服务地址
     */
    public void setRegCenterAddr(String regCenterAddr) {
        this.regCenterAddr = regCenterAddr == null ? null : regCenterAddr.trim();
    }

    /**
     * 获取是否
     *
     * @return status - 是否
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置是否
     *
     * @param status 是否
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return create_user
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_user
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }
}
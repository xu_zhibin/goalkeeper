package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_route_rule")
public class RouteRule {
    @Id
    private Integer id;

    /**
     * 路由规则名称，要求唯一
     */
    @Column(name = "route_rule_name")
    private String routeRuleName;

    /**
     * 是否启用
     */
    private Boolean status;

    private String remark;

    /**
     * 每一次创建或更新都会生成一条新的签名
     */
    private String sign;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "creata_user")
    private String creataUser;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "update_user")
    private String updateUser;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取路由规则名称，要求唯一
     *
     * @return route_rule_name - 路由规则名称，要求唯一
     */
    public String getRouteRuleName() {
        return routeRuleName;
    }

    /**
     * 设置路由规则名称，要求唯一
     *
     * @param routeRuleName 路由规则名称，要求唯一
     */
    public void setRouteRuleName(String routeRuleName) {
        this.routeRuleName = routeRuleName == null ? null : routeRuleName.trim();
    }

    /**
     * 获取是否启用
     *
     * @return status - 是否启用
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置是否启用
     *
     * @param status 是否启用
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取每一次创建或更新都会生成一条新的签名
     *
     * @return sign - 每一次创建或更新都会生成一条新的签名
     */
    public String getSign() {
        return sign;
    }

    /**
     * 设置每一次创建或更新都会生成一条新的签名
     *
     * @param sign 每一次创建或更新都会生成一条新的签名
     */
    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return creata_user
     */
    public String getCreataUser() {
        return creataUser;
    }

    /**
     * @param creataUser
     */
    public void setCreataUser(String creataUser) {
        this.creataUser = creataUser == null ? null : creataUser.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return update_user
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }
}
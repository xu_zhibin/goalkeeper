package com.github.xzb617.utils;

import java.util.Map;
import java.util.Set;

/**
 * 查询参数工具类
 * @author xzb617
 */
public class QueryParamUtil {

    /**
     * 将Map类型参数转换成查询字符串: A=B&C=D&...
     * @param paramMap
     * @return
     */
    public static String mapToQueryString(Map<String, String> paramMap) {
        if (paramMap == null || paramMap.isEmpty()) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        Set<Map.Entry<String, String>> entries = paramMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            builder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        String queryString = builder.toString();
        if (queryString.length() > 0 && queryString.endsWith("&")) {
            queryString = queryString.substring(0, queryString.length() - 1);
        }
        return queryString;
    }

}

package com.github.xzb617.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.xzb617.logs.OptLoggerAspectj;
import com.github.xzb617.logs.OptLoggerStorage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 切面的配置
 * @author xzb617
 */
@Configuration
public class AspectjConfig {

    @Bean
    public OptLoggerAspectj optLoggerAspectj(ObjectMapper objectMapper, OptLoggerStorage optLoggerStorage) {
        return new OptLoggerAspectj(optLoggerStorage, objectMapper);
    }

}

package com.github.xzb617.domain.vo.echarts;

import java.util.ArrayList;
import java.util.List;

/**
 * X轴数据
 * @author xzb
 */
public class XAxis {

    private String type = "category";

    private Boolean boundaryGap = false;

    private List<String> data = new ArrayList<>();

    public XAxis() {
    }

    public XAxis(List<String> data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getBoundaryGap() {
        return boundaryGap;
    }

    public void setBoundaryGap(Boolean boundaryGap) {
        this.boundaryGap = boundaryGap;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}

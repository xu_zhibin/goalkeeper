package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.FlowMetric;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FlowMetricMapper extends Mapper<FlowMetric> {
    List<FlowMetric> selectByStartTime(@Param("minutes") int minutes, @Param("searchText") String searchText);
}
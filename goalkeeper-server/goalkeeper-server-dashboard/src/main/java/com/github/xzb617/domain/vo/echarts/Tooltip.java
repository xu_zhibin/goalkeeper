package com.github.xzb617.domain.vo.echarts;

/**
 * 图表的提示项
 * @author xzb617
 */
public class Tooltip {

    private String trigger = "axis";

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }
}

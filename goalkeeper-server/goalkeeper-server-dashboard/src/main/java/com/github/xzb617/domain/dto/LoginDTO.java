package com.github.xzb617.domain.dto;

import javax.validation.constraints.NotBlank;

/**
 * 登录所需参数
 * @author xzb617
 */
public class LoginDTO {

    @NotBlank(message = "登录账号不能为空")
    private String username;

    @NotBlank(message = "登录密码不能为空")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

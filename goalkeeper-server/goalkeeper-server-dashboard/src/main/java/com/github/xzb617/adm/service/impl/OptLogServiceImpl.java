package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.OptLogMapper;
import com.github.xzb617.adm.service.OptLogService;
import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.common.PageTextTimeCondition;
import com.github.xzb617.domain.entity.OptLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class OptLogServiceImpl implements OptLogService {

    private final OptLogMapper optLogMapper;

    public OptLogServiceImpl(OptLogMapper optLogMapper) {
        this.optLogMapper = optLogMapper;
    }

    @Override
    @Transactional
    public void saveLogRecord(String reqURI, String optMethod, String optDesc, String optArgs, String optUser, Date optTime) {
        OptLog optLog = new OptLog();
        optLog.setRequestUri(reqURI);
        optLog.setOptMethod(optMethod);
        optLog.setOptDesc(optDesc);
        optLog.setOptArgs(optArgs);
        optLog.setOptTime(optTime);
        optLog.setOptUser(optUser);
        this.optLogMapper.insertSelective(optLog);
    }

    @Override
    public List<OptLog> getList(PageTextTimeCondition condition) {
        return this.optLogMapper.selectList(condition, null);
    }

    @Override
    public List<OptLog> getSelfList(PageTextTimeCondition condition, String username) {
        return this.optLogMapper.selectList(condition, username);
    }
}

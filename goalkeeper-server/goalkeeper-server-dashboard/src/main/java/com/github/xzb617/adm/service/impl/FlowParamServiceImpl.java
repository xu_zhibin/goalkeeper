package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.FlowParamMapper;
import com.github.xzb617.adm.service.FlowParamService;
import com.github.xzb617.domain.entity.FlowParam;
import com.github.xzb617.security.SubjectHolder;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class FlowParamServiceImpl implements FlowParamService {

    private final FlowParamMapper flowParamMapper;

    public FlowParamServiceImpl(FlowParamMapper flowParamMapper) {
        this.flowParamMapper = flowParamMapper;
    }

    @Override
    public List<FlowParam> getListByFlowId(Integer flowId) {
        Example example = new Example(FlowParam.class);
        example.createCriteria().andEqualTo("flowId", flowId);
        return this.flowParamMapper.selectByExample(example);
    }

    @Override
    public void update(Integer flowId, List<FlowParam> params) {
        // 批量删除
        this.deleteByFlowId(flowId);
        // 批量新增 TODO 后续需要实现真的批量新增
        for (FlowParam param : params) {
            param.setId(null);
            param.setFlowId(flowId);
            param.setCreateTime(new Date());
            param.setCreateUser(SubjectHolder.getContext().getUsername());
            this.flowParamMapper.insertSelective(param);
        }
    }

    @Override
    public void deleteByFlowId(Integer flowId) {
        Example example = new Example(FlowParam.class);
        example.createCriteria().andEqualTo("flowId", flowId);
        this.flowParamMapper.deleteByExample(example);
    }
}

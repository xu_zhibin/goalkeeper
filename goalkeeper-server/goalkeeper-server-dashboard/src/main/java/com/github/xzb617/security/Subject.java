package com.github.xzb617.security;

import com.github.xzb617.security.token.JwtClaims;

/**
 * 登录后的在线用户对象
 * @author xzb617
 */
public class Subject {

    private Integer userId;
    private String username;
    private String role;

    public Subject(Integer userId, String username, String role) {
        this.userId = userId;
        this.username = username;
    }

    public Subject(JwtClaims claims) {
        this.userId = claims.getUid();
        this.username = claims.getSubject();
        this.role = claims.getRole();
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }
}

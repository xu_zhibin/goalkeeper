package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_opt_log")
public class OptLog {
    @Id
    private Integer id;

    @Column(name = "request_uri")
    private String requestUri;

    @Column(name = "opt_method")
    private String optMethod;

    @Column(name = "opt_desc")
    private String optDesc;

    @Column(name = "opt_args")
    private String optArgs;

    @Column(name = "opt_time")
    private Date optTime;

    @Column(name = "opt_user")
    private String optUser;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return request_uri
     */
    public String getRequestUri() {
        return requestUri;
    }

    /**
     * @param requestUri
     */
    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri == null ? null : requestUri.trim();
    }

    /**
     * @return opt_method
     */
    public String getOptMethod() {
        return optMethod;
    }

    /**
     * @param optMethod
     */
    public void setOptMethod(String optMethod) {
        this.optMethod = optMethod == null ? null : optMethod.trim();
    }

    /**
     * @return opt_desc
     */
    public String getOptDesc() {
        return optDesc;
    }

    /**
     * @param optDesc
     */
    public void setOptDesc(String optDesc) {
        this.optDesc = optDesc == null ? null : optDesc.trim();
    }

    /**
     * @return opt_args
     */
    public String getOptArgs() {
        return optArgs;
    }

    /**
     * @param optArgs
     */
    public void setOptArgs(String optArgs) {
        this.optArgs = optArgs == null ? null : optArgs.trim();
    }

    /**
     * @return opt_time
     */
    public Date getOptTime() {
        return optTime;
    }

    /**
     * @param optTime
     */
    public void setOptTime(Date optTime) {
        this.optTime = optTime;
    }

    /**
     * @return opt_user
     */
    public String getOptUser() {
        return optUser;
    }

    /**
     * @param optUser
     */
    public void setOptUser(String optUser) {
        this.optUser = optUser == null ? null : optUser.trim();
    }
}
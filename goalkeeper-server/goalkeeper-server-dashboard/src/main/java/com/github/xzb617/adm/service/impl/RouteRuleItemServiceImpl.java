package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.RouteRuleItemMapper;
import com.github.xzb617.adm.service.RouteRuleItemService;
import com.github.xzb617.domain.entity.RouteRule;
import com.github.xzb617.domain.entity.RouteRuleItem;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class RouteRuleItemServiceImpl implements RouteRuleItemService {

    private final RouteRuleItemMapper routeRuleItemMapper;

    public RouteRuleItemServiceImpl(RouteRuleItemMapper routeRuleItemMapper) {
        this.routeRuleItemMapper = routeRuleItemMapper;
    }

    @Override
    public List<RouteRuleItem> getListByRouteRuleId(Integer routeRuleId) {
        Example example = new Example(RouteRuleItem.class);
        example.createCriteria().andEqualTo("routeRuleId", routeRuleId);
        return this.routeRuleItemMapper.selectByExample(example);
    }

    @Override
    @Transactional
    public void update(Integer routeRuleId, List<RouteRuleItem> routeRuleItems) {
        // 批量删除
        this.deleteByRouteRuleId(routeRuleId);
        // 批量新增 TODO 后续需要实现真的批量新增
        for (RouteRuleItem item : routeRuleItems) {
            item.setId(null);
            item.setRouteRuleId(routeRuleId);
            this.routeRuleItemMapper.insertSelective(item);
        }
    }

    @Override
    @Transactional
    public void deleteByRouteRuleId(Integer routeRuleId) {
        Example example = new Example(RouteRuleItem.class);
        example.createCriteria().andEqualTo("routeRuleId", routeRuleId);
        this.routeRuleItemMapper.deleteByExample(example);
    }

}

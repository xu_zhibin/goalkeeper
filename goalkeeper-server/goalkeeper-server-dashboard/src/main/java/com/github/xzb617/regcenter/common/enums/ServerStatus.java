package com.github.xzb617.regcenter.common.enums;

/**
 * 服务状态
 */
public enum ServerStatus {

    UP, DOWN

}

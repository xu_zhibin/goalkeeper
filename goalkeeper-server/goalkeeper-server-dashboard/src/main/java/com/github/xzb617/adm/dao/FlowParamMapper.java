package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.FlowParam;
import tk.mybatis.mapper.common.Mapper;

public interface FlowParamMapper extends Mapper<FlowParam> {
}
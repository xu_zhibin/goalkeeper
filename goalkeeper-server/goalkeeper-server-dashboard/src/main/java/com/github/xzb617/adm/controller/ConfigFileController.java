package com.github.xzb617.adm.controller;

import com.github.xzb617.adm.service.ConfigFileService;
import com.github.xzb617.domain.common.AjaxResponse;
import com.github.xzb617.domain.dto.ConfigFileDTO;
import com.github.xzb617.domain.entity.ConfigFile;
import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@RestController
@RequestMapping("/config/file")
public class ConfigFileController {

    private final ConfigFileService configFileService;

    public ConfigFileController(ConfigFileService configFileService) {
        this.configFileService = configFileService;
    }

    @GetMapping("/getById")
    public AjaxResponse getById(Integer id) {
        ConfigFile configFile = this.configFileService.getById(id);
        return AjaxResponse.success().message("查询成功").data(configFile);
    }

    @GetMapping("/getList")
    public AjaxResponse getList(Integer configGroupId) {
        List<ConfigFile> list = this.configFileService.getList(configGroupId);
        return AjaxResponse.success().message("查询成功").data(list);
    }

    @PostMapping("/save")
    public AjaxResponse save(@RequestBody @Validated(Insert.class) ConfigFileDTO dto) {
        this.configFileService.save(dto);
        return AjaxResponse.success().message("创建配置文件成功");
    }

    @PutMapping("/update")
    public AjaxResponse update(@RequestBody @Validated(Update.class) ConfigFileDTO dto) {
        this.configFileService.update(dto);
        return AjaxResponse.success().message("编辑配置文件成功");
    }

    @PutMapping("/release")
    public AjaxResponse release(@RequestBody ConfigFileDTO dto) {
        this.configFileService.release(dto);
        return AjaxResponse.success().message("配置文件发布成功");
    }

    @DeleteMapping("/delete")
    public AjaxResponse delete(@NotNull(message = "配置文件编号不能为空") Integer id) {
        this.configFileService.delete(id);
        return AjaxResponse.success().message("配置文件删除成功");
    }
}

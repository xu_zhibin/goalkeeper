package com.github.xzb617.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间格式化工具类
 * @author xzb617
 */
public class DateFormatUtil {

    /**
     * 将时间格式化到 时:分，如： 10:15
     * @param time 时间
     * @return
     */
    public static String formatToHourAndMinute(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(time);
    }

}

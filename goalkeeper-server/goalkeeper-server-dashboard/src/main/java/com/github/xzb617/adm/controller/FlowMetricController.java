package com.github.xzb617.adm.controller;

import com.github.xzb617.adm.service.FlowMetricService;
import com.github.xzb617.domain.common.AjaxResponse;
import com.github.xzb617.domain.common.TextCondition;
import com.github.xzb617.domain.vo.LineChartVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 限流指标数据控制器
 * <p>
 *     过滤筛选数据，用来做展示
 * </p>
 * @author xzb617
 */
@RestController
@RequestMapping("/flowMetric")
public class FlowMetricController {

    private final FlowMetricService flowMetricService;

    public FlowMetricController(FlowMetricService flowMetricService) {
        this.flowMetricService = flowMetricService;
    }

    @GetMapping("/getClient")
    public AjaxResponse getMetrics(TextCondition condition) {
        List<LineChartVO> lineCharts = this.flowMetricService.getClientMetrics(condition);
        return AjaxResponse.success().message("查询成功").data(lineCharts);
    }


}

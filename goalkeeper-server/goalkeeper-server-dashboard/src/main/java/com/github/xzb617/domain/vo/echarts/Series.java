package com.github.xzb617.domain.vo.echarts;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据项
 * @author xzb617
 */
public class Series {

    private String name;
    private String type;
    // 折线是否平滑
    private Boolean smooth = true;
    private List<Long> data = new ArrayList<>();

    public Series() {
    }

    public Series(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Long> getData() {
        return data;
    }

    public void setData(List<Long> data) {
        this.data = data;
    }

    public Boolean getSmooth() {
        return smooth;
    }

    public void setSmooth(Boolean smooth) {
        this.smooth = smooth;
    }
}

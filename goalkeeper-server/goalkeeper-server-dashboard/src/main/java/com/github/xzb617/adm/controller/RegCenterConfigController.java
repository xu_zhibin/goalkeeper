package com.github.xzb617.adm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xzb617.adm.service.RegCenterConfigService;
import com.github.xzb617.domain.common.AjaxResponse;
import com.github.xzb617.domain.common.Page;
import com.github.xzb617.domain.common.PageData;
import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.RegCenterConfigDTO;
import com.github.xzb617.domain.entity.RegCenterConfig;
import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/regcenter/config")
public class RegCenterConfigController {

    private final RegCenterConfigService regCenterConfigService;

    public RegCenterConfigController(RegCenterConfigService regCenterConfigService) {
        this.regCenterConfigService = regCenterConfigService;
    }

    @GetMapping("/getList")
    public AjaxResponse getList(PageTextCondition condition) {
        PageHelper.startPage(condition.getPageNum(), condition.getPageSize());
        List<RegCenterConfig> details = this.regCenterConfigService.getList(condition);
        PageInfo<RegCenterConfig> pageInfo = new PageInfo<>(details);
        PageData<RegCenterConfig> pageData = Page.toData(pageInfo);
        return AjaxResponse.success().message("查询成功").data(pageData);
    }

    @PostMapping("/save")
    public AjaxResponse save(@RequestBody @Validated(Insert.class) RegCenterConfigDTO dto) {
        this.regCenterConfigService.save(dto);
        return AjaxResponse.success().message("创建成功");
    }

    @PutMapping("/update")
    public AjaxResponse update(@RequestBody @Validated(Update.class) RegCenterConfigDTO dto) {
        this.regCenterConfigService.update(dto);
        return AjaxResponse.success().message("编辑成功");
    }

    @PutMapping("/modifyStatus")
    public AjaxResponse modifyStatus(@RequestBody RegCenterConfigDTO dto) {
        this.regCenterConfigService.modifyStatus(dto);
        return AjaxResponse.success().message("变更成功");
    }

    @DeleteMapping("/deleteById")
    public AjaxResponse deleteById(Integer id) {
        this.regCenterConfigService.deleteById(id);
        return AjaxResponse.success().message("删除成功");
    }
}

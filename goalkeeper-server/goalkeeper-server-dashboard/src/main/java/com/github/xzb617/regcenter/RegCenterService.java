package com.github.xzb617.regcenter;

import com.github.xzb617.domain.condition.RegCenterInstanceCondition;
import com.github.xzb617.regcenter.common.Application;
import com.github.xzb617.regcenter.common.Instance;
import com.github.xzb617.regcenter.common.enums.ServerStatus;

import java.util.List;
import java.util.Map;

public interface RegCenterService {

    List<Application> getApps(String serverUrl);

    List<Instance> getInstances(String serverUrl, String appId);

    boolean modifyMetadata(String serverUrl, String appId, String instanceId, Map<String, String> metadata);

    boolean modifyStatus(String serverUrl, String appId, String instanceId, ServerStatus serverStatus);

}

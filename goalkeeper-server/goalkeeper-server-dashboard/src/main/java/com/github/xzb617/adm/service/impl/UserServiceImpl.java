package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.UserMapper;
import com.github.xzb617.adm.service.UserService;
import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.ModifySelfPwdDTO;
import com.github.xzb617.domain.dto.UserDTO;
import com.github.xzb617.domain.entity.User;
import com.github.xzb617.except.ServiceException;
import com.github.xzb617.props.SecurityProperties;
import com.github.xzb617.security.Subject;
import com.github.xzb617.security.SubjectHolder;
import com.github.xzb617.utils.PasswordEncryptor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    /**
     * 未配置初始化密码的情况下，采用此默认初始密码
     */
    private final static String USER_INIT_PASSWORD = "666666";

    private final UserMapper userMapper;
    private final SecurityProperties securityProperties;

    public UserServiceImpl(UserMapper userMapper, SecurityProperties securityProperties) {
        this.userMapper = userMapper;
        this.securityProperties = securityProperties;
    }

    @Override
    public User getById(Integer id) {
        return this.userMapper.selectByPrimaryKey(id);
    }

    @Override
    public User getByName(String username) {
        User condition = new User();
        condition.setUsername(username);
        return this.userMapper.selectOne(condition);
    }

    @Override
    public List<User> getList(PageTextCondition condition) {
        return this.userMapper.selectList(condition);
    }

    @Override
    @Transactional
    public void save(UserDTO dto) {
        // 判断用户名、手机号是否已被使用
        long nameCount = this.userMapper.countByUsername(dto.getUsername());
        if (nameCount > 0) {
            throw new ServiceException("用户名已被使用");
        }
        long phoneCount = this.userMapper.countByPhoneNumber(dto.getPhoneNumber());
        if (phoneCount > 0) {
            throw new ServiceException("手机号已被注册");
        }
        // 获取用户初始密码参数
        String userInitPassword = this.securityProperties.getUserInitPassword();
        String initPassword = userInitPassword==null?USER_INIT_PASSWORD:userInitPassword;
        // 加密密码
        String salt = PasswordEncryptor.generateSalt(6);
        String encryptedInitPassword = PasswordEncryptor.encrypt(initPassword, securityProperties.getUserPasswordEncryptedSecret(), salt);
        // 新增用户
        User user = new User();
        BeanUtils.copyProperties(dto, user);
        user.setPassword(encryptedInitPassword);
        user.setSalt(salt);
        user.setCreateTime(new Date());
        user.setCreateUser(SubjectHolder.getContext().getUsername());
        this.userMapper.insertSelective(user);
    }

    @Override
    @Transactional
    public void update(UserDTO dto) {
        // 获取要编辑的用户
        User user = this.getById(dto.getId());
        if (user == null) {
            throw new ServiceException("要编辑的用户不存在或已被删除");
        }
        // 判断手机号是否已被使用
        if (!dto.getPhoneNumber().equalsIgnoreCase(user.getPhoneNumber())) {
            long phoneCount = this.userMapper.countByPhoneNumber(dto.getPhoneNumber());
            if (phoneCount > 0) {
                throw new ServiceException("手机号已被注册");
            }
        }
        // 编辑用户
        BeanUtils.copyProperties(dto, user, "id","username");
        user.setUpdateTime(new Date());
        user.setUpdateUser(SubjectHolder.getContext().getUsername());
        this.userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        User user = this.getById(id);
        if (user == null) {
            throw new ServiceException("要删除的用户不存在或已被删除");
        }
        this.userMapper.deleteByPrimaryKey(id);
    }

    @Override
    @Transactional
    public void resetPwd(Integer id) {
        // 获取用户初始密码参数
        String userInitPassword = this.securityProperties.getUserInitPassword();
        String initPassword = userInitPassword==null?USER_INIT_PASSWORD:userInitPassword;
        // 获取要重置的用户
        User user = this.getById(id);
        if (user == null) {
            throw new ServiceException("要重置密码的用户不存在或已被删除");
        }
        // 加密密码
        String encryptedInitPassword = PasswordEncryptor.encrypt(initPassword, securityProperties.getUserPasswordEncryptedSecret(), user.getSalt());
        // 重置密码
        user.setPassword(encryptedInitPassword);
        user.setUpdateTime(new Date());
        user.setUpdateUser(SubjectHolder.getContext().getUsername());
        this.userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    @Transactional
    public void modifySelfPwd(ModifySelfPwdDTO dto) {
        // 获取用户
        Integer loginUid = SubjectHolder.getContext().getUserId();
        User user = this.getById(loginUid);
        if (user == null) {
            throw new ServiceException("您的账号不存在或已被删除");
        }
        // 校验密码是否合规
        boolean matched =PasswordEncryptor.match(user.getPassword(), dto.getOldPassword(), this.securityProperties.getUserPasswordEncryptedSecret(), user.getSalt());
        if (!matched) {
            throw new ServiceException("原始密码错误");
        }
        // 加密密码
        String encryptedNewPassword = PasswordEncryptor.encrypt(dto.getNewPassword(), this.securityProperties.getUserPasswordEncryptedSecret(), user.getSalt());
        // 更新密码
        user.setPassword(encryptedNewPassword);
        user.setUpdateTime(new Date());
        user.setUpdateUser(SubjectHolder.getContext().getUsername());
        this.userMapper.updateByPrimaryKeySelective(user);
    }

}

package com.github.xzb617.regcenter.eureka.dto;

import java.util.ArrayList;
import java.util.List;

public class EurekaApplication {

    private String name;

    private List<EurekaInstance> instance = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EurekaInstance> getInstance() {
        return instance;
    }

    public void setInstance(List<EurekaInstance> instance) {
        this.instance = instance;
    }

}

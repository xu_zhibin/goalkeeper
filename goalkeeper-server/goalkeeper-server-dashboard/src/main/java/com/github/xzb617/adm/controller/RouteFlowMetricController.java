package com.github.xzb617.adm.controller;

import com.github.xzb617.adm.service.RouteFlowMetricService;
import com.github.xzb617.domain.common.AjaxResponse;
import com.github.xzb617.domain.common.TextCondition;
import com.github.xzb617.domain.vo.LineChartVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 路由流量指标数据控制器
 * <p>
 *     按服务分组，每个服务的不同实例进行流量展示
 * </p>
 * @author xzb617
 */
@RestController
@RequestMapping("/routeFlowMetric")
public class RouteFlowMetricController {

    private final RouteFlowMetricService routeFlowMetricService;

    public RouteFlowMetricController(RouteFlowMetricService routeFlowMetricService) {
        this.routeFlowMetricService = routeFlowMetricService;
    }

    @GetMapping("/getRouteMetrics")
    public AjaxResponse getRouteMetrics(TextCondition condition) {
        List<LineChartVO> lineCharts = this.routeFlowMetricService.getClientRouteMetrics(condition);
        return AjaxResponse.success().message("查询成功").data(lineCharts);
    }

}

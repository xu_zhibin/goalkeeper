package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.ConfigFile;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface ConfigFileMapper extends Mapper<ConfigFile> {

    ConfigFile selectById(@Param("id") Integer id);

    ConfigFile selectByClient(@Param("configGroupName") String configGroupName, @Param("configFileName") String configFileName);

}
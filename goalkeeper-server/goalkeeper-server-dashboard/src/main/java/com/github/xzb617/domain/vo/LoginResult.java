package com.github.xzb617.domain.vo;

import java.util.Date;
import java.util.Set;

public class LoginResult {

    private String token;

    private Integer userId;

    private String username;

    private Set<String> roles;

    private Date loginTime;

    public LoginResult() {
    }

    public LoginResult(String token, Integer userId, String username, Date loginTime, Set<String> roles) {
        this.token = token;
        this.userId = userId;
        this.username = username;
        this.loginTime = loginTime;
        this.roles = roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}

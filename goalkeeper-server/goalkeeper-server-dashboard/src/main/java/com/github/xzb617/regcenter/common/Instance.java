package com.github.xzb617.regcenter.common;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 通用的实例模型
 * <p>
 *     屏蔽不同注册中心的实例数据模型
 * </p>
 * @author xzb617
 */
public class Instance {

    private String instanceId;

    private String appId;

    private String hostName;

    private String ipAddr;

    private Integer port;

    private Boolean status;

    // 当前实例的状态是否处于变更中
    private Boolean statusChanging;

    private Map<String, String> metadata = new HashMap<>();

    private Date lastUpdatedTimestamp;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public Date getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
    }

    public Boolean getStatusChanging() {
        return statusChanging;
    }

    public void setStatusChanging(Boolean statusChanging) {
        this.statusChanging = statusChanging;
    }
}

package com.github.xzb617.domain.vo;

import com.github.xzb617.domain.entity.RouteRule;
import com.github.xzb617.domain.entity.RouteRuleItem;

import java.util.ArrayList;
import java.util.List;

public class RouteVO {

    private RouteRule routeRule;

    private List<RouteRuleItem> routeRuleItems = new ArrayList<>();

    public RouteVO() {
    }

    public RouteVO(RouteRule routeRule, List<RouteRuleItem> routeRuleItems) {
        this.routeRule = routeRule;
        this.routeRuleItems = routeRuleItems;
    }

    public RouteRule getRouteRule() {
        return routeRule;
    }

    public void setRouteRule(RouteRule routeRule) {
        this.routeRule = routeRule;
    }

    public List<RouteRuleItem> getRouteRuleItems() {
        return routeRuleItems;
    }

    public void setRouteRuleItems(List<RouteRuleItem> routeRuleItems) {
        this.routeRuleItems = routeRuleItems;
    }
}

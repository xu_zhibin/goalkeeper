package com.github.xzb617.adm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xzb617.adm.service.ConfigGroupService;
import com.github.xzb617.domain.common.AjaxResponse;
import com.github.xzb617.domain.common.Page;
import com.github.xzb617.domain.common.PageData;
import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.ConfigGroupDTO;
import com.github.xzb617.domain.entity.ConfigGroup;
import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@RestController
@RequestMapping("/config/group")
public class ConfigGroupController {

    private final ConfigGroupService configGroupService;

    public ConfigGroupController(ConfigGroupService configGroupService) {
        this.configGroupService = configGroupService;
    }

    @GetMapping("/getList")
    public AjaxResponse getList(PageTextCondition condition) {
        PageHelper.startPage(condition.getPageNum(), condition.getPageSize());
        List<ConfigGroup> details = this.configGroupService.getList(condition);
        PageInfo<ConfigGroup> pageInfo = new PageInfo<>(details);
        PageData<ConfigGroup> pageData = Page.toData(pageInfo);
        return AjaxResponse.success().message("查询成功").data(pageData);
    }

    @GetMapping("/getAll")
    public AjaxResponse getAll() {
        List<ConfigGroup> list =this.configGroupService.getAll();
        return AjaxResponse.success().message("查询成功").data(list);
    }

    @PostMapping("/save")
    public AjaxResponse save(@RequestBody @Validated(Insert.class) ConfigGroupDTO dto) {
        this.configGroupService.save(dto);
        return AjaxResponse.success().message("创建配置分组成功");
    }

    @PutMapping("/update")
    public AjaxResponse update(@RequestBody @Validated(Update.class) ConfigGroupDTO dto) {
        this.configGroupService.update(dto);
        return AjaxResponse.success().message("编辑配置分组成功");
    }

    @DeleteMapping("/delete")
    public AjaxResponse delete(@NotNull(message = "配置分组编号不能为空") Integer id) {
        this.configGroupService.delete(id);
        return AjaxResponse.success().message("删除配置分组成功");
    }
}

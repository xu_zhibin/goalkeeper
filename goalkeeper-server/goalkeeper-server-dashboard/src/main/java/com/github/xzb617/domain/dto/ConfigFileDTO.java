package com.github.xzb617.domain.dto;

import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 配置文件新增或更新模型
 * @author xzb617
 */
public class ConfigFileDTO {

    @NotNull(message = "配置文件编号不能为空", groups = {Update.class})
    private Integer id;

    @NotNull(message = "配置分组编号不能为空", groups = {Insert.class, Update.class})
    private Integer configGroupId;

    @NotBlank(message = "配置文件名不能为空", groups = {Insert.class, Update.class})
    @Length(max = 50, message = "配置文件名长度不允许超过50个字", groups = {Insert.class, Update.class})
    private String configName;

    @NotNull(message = "配置文件类型不能为空", groups = {Insert.class, Update.class})
    private String fileExtension;

    @Length(max = 100, message = "配置文件备注长度不允许超过100个字", groups = {Insert.class, Update.class})
    private String remark;

    private String configContent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getConfigGroupId() {
        return configGroupId;
    }

    public void setConfigGroupId(Integer configGroupId) {
        this.configGroupId = configGroupId;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getConfigContent() {
        return configContent;
    }

    public void setConfigContent(String configContent) {
        this.configContent = configContent;
    }
}

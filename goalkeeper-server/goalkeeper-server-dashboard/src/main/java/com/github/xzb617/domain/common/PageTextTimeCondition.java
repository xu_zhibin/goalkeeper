package com.github.xzb617.domain.common;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 包含通用时间区间条件
 * @author xzb617
 */
public class PageTextTimeCondition extends PageTextCondition {

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date bgnTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    public Date getBgnTime() {
        return bgnTime;
    }

    public void setBgnTime(Date bgnTime) {
        this.bgnTime = bgnTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}

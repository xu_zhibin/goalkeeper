package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.RegCenterConfigMapper;
import com.github.xzb617.adm.service.RegCenterConfigService;
import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.RegCenterConfigDTO;
import com.github.xzb617.domain.entity.ConfigGroup;
import com.github.xzb617.domain.entity.RegCenterConfig;
import com.github.xzb617.except.ServiceException;
import com.github.xzb617.security.SubjectHolder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class RegCenterConfigServiceImpl implements RegCenterConfigService {

    private final RegCenterConfigMapper regCenterConfigMapper;

    public RegCenterConfigServiceImpl(RegCenterConfigMapper regCenterConfigMapper) {
        this.regCenterConfigMapper = regCenterConfigMapper;
    }

    @Override
    public RegCenterConfig getCurrent() {
        Example example = new Example(RegCenterConfig.class);
        example.createCriteria()
                .andEqualTo("status", true);
        return this.regCenterConfigMapper.selectOneByExample(example);
    }

    @Override
    public RegCenterConfig getByName(String regCenterName) {
        Example example = new Example(RegCenterConfig.class);
        example.createCriteria()
                .andEqualTo("regCenterName", regCenterName);
        return this.regCenterConfigMapper.selectOneByExample(example);
    }

    @Override
    public List<RegCenterConfig> getList(PageTextCondition condition) {
        Example example = new Example(RegCenterConfig.class);
        Example.Criteria criteria = example.createCriteria();
        if (condition.isNotEmptyOrNull()) {
            criteria.andLike("regCenterName", condition.like())
                    .orLike("regCenterAddr", condition.like());
        }
        example.orderBy("status").desc();
        return this.regCenterConfigMapper.selectByExample(example);
    }

    @Override
    @Transactional
    public void save(RegCenterConfigDTO dto) {
        // 判断分组名称是否已存在
        RegCenterConfig var1 = this.getByName(dto.getRegCenterName());
        if (var1 != null) {
            throw new ServiceException("注册中心名称已被使用");
        }
        // 赋值并保存
        var1 = new RegCenterConfig();
        BeanUtils.copyProperties(dto, var1);
        var1.setStatus(false);
        var1.setCreateUser(SubjectHolder.getContext().getUsername());
        var1.setCreateTime(new Date());
        this.regCenterConfigMapper.insertSelective(var1);
    }

    @Override
    @Transactional
    public void update(RegCenterConfigDTO dto) {
        // 判断分组是否存在
        RegCenterConfig var1 = this.regCenterConfigMapper.selectByPrimaryKey(dto.getId());
        if (var1 == null) {
            throw new ServiceException("注册中心不存在");
        }
        // 判断分组名称是否已被使用
        RegCenterConfig var2 = this.getByName(dto.getRegCenterName());
        if (var2!=null && var2.getId()!=var1.getId()) {
            throw new ServiceException("注册中心名称已被使用");
        }
        // 赋值并更新
        BeanUtils.copyProperties(dto, var1);
        var1.setUpdateTime(new Date());
        var1.setUpdateUser(SubjectHolder.getContext().getUsername());
        this.regCenterConfigMapper.updateByPrimaryKeySelective(var1);
    }

    @Override
    @Transactional
    public void modifyStatus(RegCenterConfigDTO dto) {
        // 判断分组是否存在
        RegCenterConfig var1 = this.regCenterConfigMapper.selectByPrimaryKey(dto.getId());
        if (var1 == null) {
            throw new ServiceException("注册中心不存在");
        }
        // 获取原先默认
        RegCenterConfig defaultConfig = this.getCurrent();
        if (defaultConfig != null) {
            defaultConfig.setStatus(false);
            this.regCenterConfigMapper.updateByPrimaryKeySelective(defaultConfig);
        }
        // 设置默认
        var1.setStatus(true);
        this.regCenterConfigMapper.updateByPrimaryKeySelective(var1);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        // 删除分组
        RegCenterConfig var1 = this.regCenterConfigMapper.selectByPrimaryKey(id);
        if (var1 == null) {
            throw new ServiceException("注册中心不存在");
        }
        this.regCenterConfigMapper.deleteByPrimaryKey(id);
    }
}

package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.ConfigGroupMapper;
import com.github.xzb617.adm.service.ConfigFileService;
import com.github.xzb617.adm.service.ConfigGroupService;
import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.ConfigGroupDTO;
import com.github.xzb617.domain.entity.ConfigGroup;
import com.github.xzb617.except.ServiceException;
import com.github.xzb617.security.SubjectHolder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class ConfigGroupServiceImpl implements ConfigGroupService {

    private final ConfigGroupMapper configGroupMapper;
    private final ConfigFileService configFileService;

    public ConfigGroupServiceImpl(ConfigGroupMapper configGroupMapper, ConfigFileService configFileService) {
        this.configGroupMapper = configGroupMapper;
        this.configFileService = configFileService;
    }

    @Override
    public ConfigGroup getById(Integer id) {
        return this.configGroupMapper.selectByPrimaryKey(id);
    }

    @Override
    public ConfigGroup getByName(String groupName) {
        Example example = new Example(ConfigGroup.class);
        example.createCriteria().andLike("groupName", groupName);
        return this.configGroupMapper.selectOneByExample(example);
    }

    @Override
    public List<ConfigGroup> getList(PageTextCondition condition) {
        Example example = new Example(ConfigGroup.class);
        Example.Criteria criteria = example.createCriteria();
        if (condition.isNotEmptyOrNull()) {
            criteria.andLike("groupName", condition.like())
                    .orLike("groupDesc", condition.like());
        }
        return this.configGroupMapper.selectByExample(example);
    }

    @Override
    public List<ConfigGroup> getAll() {
        return this.configGroupMapper.selectAll();
    }

    @Override
    @Transactional
    public void save(ConfigGroupDTO dto) {
        // 判断分组名称是否已存在
        ConfigGroup var1 = this.getByName(dto.getGroupName());
        if (var1 != null) {
            throw new ServiceException("配置分组名称已被使用");
        }
        // 赋值并保存
        var1 = new ConfigGroup();
        BeanUtils.copyProperties(dto, var1);
        var1.setCreateUser(SubjectHolder.getContext().getUsername());
        var1.setCreateTime(new Date());
        this.configGroupMapper.insertSelective(var1);
    }

    @Override
    @Transactional
    public void update(ConfigGroupDTO dto) {
        // 判断分组是否存在
        ConfigGroup var1 = this.getById(dto.getId());
        if (var1 == null) {
            throw new ServiceException("配置分组不存在");
        }
        // 判断分组名称是否已被使用
        ConfigGroup var2 = this.getByName(dto.getGroupName());
        if (var2!=null && var2.getId()!=var1.getId()) {
            throw new ServiceException("配置分组名称已被使用");
        }
        // 赋值并更新
        BeanUtils.copyProperties(dto, var1);
        var1.setUpdateTime(new Date());
        var1.setUpdateUser(SubjectHolder.getContext().getUsername());
        this.configGroupMapper.updateByPrimaryKeySelective(var1);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        // 删除分组内配置文件
        this.configFileService.deleteByGroup(id);
        // 删除分组
        ConfigGroup var1 = this.getById(id);
        if (var1 == null) {
            throw new ServiceException("配置分组不存在");
        }
        this.configGroupMapper.deleteByPrimaryKey(id);
    }
}

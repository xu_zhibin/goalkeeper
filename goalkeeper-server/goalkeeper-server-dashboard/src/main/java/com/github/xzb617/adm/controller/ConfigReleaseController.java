package com.github.xzb617.adm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xzb617.adm.service.ConfigReleaseService;
import com.github.xzb617.domain.common.AjaxResponse;
import com.github.xzb617.domain.common.Page;
import com.github.xzb617.domain.common.PageData;
import com.github.xzb617.domain.condition.ConfigReleaseCondition;
import com.github.xzb617.domain.entity.ConfigRelease;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/config/release")
public class ConfigReleaseController {

    private final ConfigReleaseService configReleaseService;

    public ConfigReleaseController(ConfigReleaseService configReleaseService) {
        this.configReleaseService = configReleaseService;
    }

    @GetMapping("/getList")
    public AjaxResponse getList(ConfigReleaseCondition condition) {
        PageHelper.startPage(condition.getPageNum(), condition.getPageSize());
        List<ConfigRelease> details = this.configReleaseService.getList(condition);
        PageInfo<ConfigRelease> pageInfo = new PageInfo<>(details);
        PageData<ConfigRelease> pageData = Page.toData(pageInfo);
        return AjaxResponse.success().message("查询成功").data(pageData);
    }

}

package com.github.xzb617.adm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xzb617.adm.service.OptLogService;
import com.github.xzb617.domain.common.*;
import com.github.xzb617.domain.entity.OptLog;
import com.github.xzb617.security.SubjectHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 操作日志控制器
 * @author xzb617
 */
@RequestMapping("/optlog")
@RestController
public class OptLogController {

    private final OptLogService optLogService;

    public OptLogController(OptLogService optLogService) {
        this.optLogService = optLogService;
    }

    /**
     * 查询所有操作日志
     * @param condition 查询条件
     * @return
     */
    @GetMapping("/getList")
    public AjaxResponse getList(PageTextTimeCondition condition) {
        PageHelper.startPage(condition.getPageNum(), condition.getPageSize());
        List<OptLog> list = this.optLogService.getList(condition);
        PageInfo<OptLog> pageInfo = new PageInfo<>(list);
        PageData pageData = Page.toData(pageInfo);
        return AjaxResponse.success().message("查询成功").data(pageData);
    }

    /**
     * 查询登录用户自己的操作日志(最近10条)
     * @param condition 查询条件
     * @return
     */
    @GetMapping("/getSelfList")
    public AjaxResponse getSelfList(PageTextTimeCondition condition) {
        String username = SubjectHolder.getContext().getUsername();
        PageHelper.startPage(1, 10);
        List<OptLog> list = this.optLogService.getSelfList(condition, username);
        PageInfo<OptLog> pageInfo = new PageInfo<>(list);
        PageData pageData = Page.toData(pageInfo);
        return AjaxResponse.success().message("查询成功").data(pageData);
    }

}

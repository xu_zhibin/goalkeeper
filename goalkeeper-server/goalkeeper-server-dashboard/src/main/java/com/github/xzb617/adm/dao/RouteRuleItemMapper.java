package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.RouteRuleItem;
import tk.mybatis.mapper.common.Mapper;

public interface RouteRuleItemMapper extends Mapper<RouteRuleItem> {
}
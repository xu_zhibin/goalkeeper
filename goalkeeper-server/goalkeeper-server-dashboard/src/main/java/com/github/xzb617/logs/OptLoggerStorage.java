package com.github.xzb617.logs;

import java.util.Date;

/**
 * 操作日志数据保存接口
 * @author xzb617
 */
public interface OptLoggerStorage {

    void saveLogRecord(String reqURI, String optMethod, String optDesc, String optArgs, String optUser, Date optTime);

}

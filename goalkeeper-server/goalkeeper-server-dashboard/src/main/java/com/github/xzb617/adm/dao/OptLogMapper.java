package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.common.PageTextTimeCondition;
import com.github.xzb617.domain.entity.OptLog;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface OptLogMapper extends Mapper<OptLog> {

    List<OptLog> selectList(@Param("condition") PageTextTimeCondition condition, @Param("username") String username);

}
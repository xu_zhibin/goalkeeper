package com.github.xzb617.security.perms;

/**
 * 用户角色
 * @author xzb617
 */
public enum Role {
    SA("SA"),
    CA("CA");

    private String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

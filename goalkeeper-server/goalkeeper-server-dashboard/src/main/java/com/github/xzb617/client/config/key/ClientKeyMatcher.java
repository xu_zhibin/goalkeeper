package com.github.xzb617.client.config.key;

public class ClientKeyMatcher {

    public static boolean matches(ClientKeyType clientKeyType, String clientKey, String target) {
        if (clientKeyType == ClientKeyType.CONFIG) {
            return clientKey.startsWith(target + "&");
        }
        if (clientKeyType == ClientKeyType.ROUTE) {
            return clientKey.endsWith("&" + target);
        }
        return clientKey.contains("&" + target + "&");
    }

}

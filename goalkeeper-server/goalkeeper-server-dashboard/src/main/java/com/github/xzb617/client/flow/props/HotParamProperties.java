package com.github.xzb617.client.flow.props;


import com.github.xzb617.client.flow.dto.HotParamRule;

import java.util.HashMap;
import java.util.Map;

public class HotParamProperties {

    private Boolean enabled = true;

    private Map<String, HotParamRule> rules = new HashMap<>();

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, HotParamRule> getRules() {
        return rules;
    }

    public void setRules(Map<String, HotParamRule> rules) {
        this.rules = rules;
    }

}

package com.github.xzb617.domain.dto;

import com.github.xzb617.valid.group.Insert;

import javax.validation.constraints.NotBlank;

/**
 * 更改自己的密码
 * @author xzb617
 */
public class ModifySelfPwdDTO {

    @NotBlank(message = "原始密码不能为空")
    private String oldPassword;

    @NotBlank(message = "新的密码不能为空")
    private String newPassword;

    @NotBlank(message = "确认密码不能为空")
    private String repPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepPassword() {
        return repPassword;
    }

    public void setRepPassword(String repPassword) {
        this.repPassword = repPassword;
    }
}

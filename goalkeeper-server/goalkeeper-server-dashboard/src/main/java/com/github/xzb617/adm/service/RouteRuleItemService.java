package com.github.xzb617.adm.service;

import com.github.xzb617.domain.entity.RouteRuleItem;

import java.util.List;

public interface RouteRuleItemService {
    List<RouteRuleItem> getListByRouteRuleId(Integer id);

    void update(Integer routeRuleId, List<RouteRuleItem> routeRuleItems);

    void deleteByRouteRuleId(Integer routeRuleId);
}

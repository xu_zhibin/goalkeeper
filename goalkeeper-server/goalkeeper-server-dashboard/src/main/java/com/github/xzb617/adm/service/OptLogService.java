package com.github.xzb617.adm.service;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.common.PageTextTimeCondition;
import com.github.xzb617.domain.entity.OptLog;
import com.github.xzb617.logs.OptLoggerStorage;

import java.util.List;

public interface OptLogService extends OptLoggerStorage {

    List<OptLog> getList(PageTextTimeCondition condition);

    List<OptLog> getSelfList(PageTextTimeCondition condition, String username);

}

package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.ConfigGroup;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface ConfigGroupMapper extends Mapper<ConfigGroup> {



}
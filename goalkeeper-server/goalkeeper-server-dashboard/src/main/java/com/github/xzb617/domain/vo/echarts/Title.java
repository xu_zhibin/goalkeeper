package com.github.xzb617.domain.vo.echarts;

/**
 * echarts 图表标题项
 * @author xzb617
 */
public class Title {

    private String text;

    private TextStyle textStyle;

    public static class TextStyle {

        private String fontStyle = "normal";

        private String fontWeight = "normal";

        private String fontSize = "14";

        public String getFontStyle() {
            return fontStyle;
        }

        public void setFontStyle(String fontStyle) {
            this.fontStyle = fontStyle;
        }

        public String getFontWeight() {
            return fontWeight;
        }

        public void setFontWeight(String fontWeight) {
            this.fontWeight = fontWeight;
        }

        public String getFontSize() {
            return fontSize;
        }

        public void setFontSize(String fontSize) {
            this.fontSize = fontSize;
        }
    }


    public Title() {
    }

    public Title(String text, TextStyle textStyle) {
        this.text = text;
        this.textStyle = textStyle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }
}

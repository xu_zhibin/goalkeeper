package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.condition.ConfigReleaseCondition;
import com.github.xzb617.domain.entity.ConfigRelease;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface ConfigReleaseMapper extends Mapper<ConfigRelease> {

    List<ConfigRelease> selectList(@Param("condition") ConfigReleaseCondition condition);

}
package com.github.xzb617.domain.vo.echarts;

/**
 * y轴数据
 * @author xzb617
 */
public class YAxis {

    private String type =  "value";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

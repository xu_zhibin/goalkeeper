package com.github.xzb617.constant;

public class SuperAdminConstant {

    /** 系统默认的超级管理员，账户名不可更改，且只有一个超级管理员 */
    public final static String SA = "goalkeeper";

}

package com.github.xzb617.regcenter.common;

/**
 * 通用的应用
 * <p>
 *     屏蔽各个注册中心的应用数据模型
 * </p>
 * @author xzb617
 */
public class Application {

    // 应用ID
    private String id;

    // 实例数
    private Integer instanceCount;

    // 在线实例数据
    private Integer upInstanceCount;

    // 非在线实例数
    private Integer downInstanceCount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getInstanceCount() {
        return instanceCount;
    }

    public void setInstanceCount(Integer instanceCount) {
        this.instanceCount = instanceCount;
    }

    public Integer getUpInstanceCount() {
        return upInstanceCount;
    }

    public void setUpInstanceCount(Integer upInstanceCount) {
        this.upInstanceCount = upInstanceCount;
    }

    public Integer getDownInstanceCount() {
        return downInstanceCount;
    }

    public void setDownInstanceCount(Integer downInstanceCount) {
        this.downInstanceCount = downInstanceCount;
    }
}

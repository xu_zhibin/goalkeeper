package com.github.xzb617.regcenter.eureka.dto;

import java.util.ArrayList;
import java.util.List;

public class EurekaApplications {

    private String versions__delta;

    private String apps__hashcode;

    private List<EurekaApplication> application = new ArrayList<>();

    public String getVersions__delta() {
        return versions__delta;
    }

    public void setVersions__delta(String versions__delta) {
        this.versions__delta = versions__delta;
    }

    public String getApps__hashcode() {
        return apps__hashcode;
    }

    public void setApps__hashcode(String apps__hashcode) {
        this.apps__hashcode = apps__hashcode;
    }

    public List<EurekaApplication> getApplication() {
        return application;
    }

    public void setApplication(List<EurekaApplication> application) {
        this.application = application;
    }

}

package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.Flow;
import tk.mybatis.mapper.common.Mapper;

public interface FlowMapper extends Mapper<Flow> {
}
package com.github.xzb617.adm.service;

import com.github.xzb617.client.flow.dto.ClientFlowMetric;
import com.github.xzb617.domain.common.TextCondition;
import com.github.xzb617.domain.vo.LineChartVO;

import java.util.List;

public interface FlowMetricService {

    /**
     * 保存客户端上报的流控指标
     * @param clientFlowMetric 客户端流控指标
     */
    void saveClientMetric(ClientFlowMetric clientFlowMetric);

    /**
     * 查询流控指标
     * @return
     */
    List<LineChartVO> getClientMetrics(TextCondition condition);

}

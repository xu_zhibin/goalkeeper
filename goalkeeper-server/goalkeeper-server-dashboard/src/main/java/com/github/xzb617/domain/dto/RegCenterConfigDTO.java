package com.github.xzb617.domain.dto;

import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RegCenterConfigDTO {

    @NotNull(message = "注册中心编号不能为空", groups = {Update.class})
    private Integer id;

    @NotBlank(message = "注册中心名不能为空", groups = {Insert.class, Update.class})
    @Length(max = 50, message = "注册中心名长度不允许超过50个字", groups = {Insert.class, Update.class})
    private String regCenterName;

    /**
     * 注册中心类型，目前仅支持EUREKA
     */
    @NotNull(message = "注册中心类型不能为空", groups = {Insert.class, Update.class})
    private String regCenterType;

    /**
     * 服务地址
     */
    @NotBlank(message = "注册中心地址不能为空", groups = {Insert.class, Update.class})
    @Length(max = 255, message = "注册中心地址长度不允许超过255个字", groups = {Insert.class, Update.class})
    private String regCenterAddr;

    /**
     * 是否为当前默认
     */
    private Boolean status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegCenterName() {
        return regCenterName;
    }

    public void setRegCenterName(String regCenterName) {
        this.regCenterName = regCenterName;
    }

    public String getRegCenterType() {
        return regCenterType;
    }

    public void setRegCenterType(String regCenterType) {
        this.regCenterType = regCenterType;
    }

    public String getRegCenterAddr() {
        return regCenterAddr;
    }

    public void setRegCenterAddr(String regCenterAddr) {
        this.regCenterAddr = regCenterAddr;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}

package com.github.xzb617.adm.service;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.ConfigGroupDTO;
import com.github.xzb617.domain.entity.ConfigGroup;

import java.util.List;

public interface ConfigGroupService {

    ConfigGroup getById(Integer id);

    ConfigGroup getByName(String groupName);

    List<ConfigGroup> getList(PageTextCondition condition);

    List<ConfigGroup> getAll();

    void save(ConfigGroupDTO dto);

    void update(ConfigGroupDTO dto);

    void delete(Integer id);

}

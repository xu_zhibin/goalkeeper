package com.github.xzb617.domain.dto;

import com.github.xzb617.valid.group.Insert;
import com.github.xzb617.valid.group.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 配置分组新增或删除模型
 * @author xzb617
 */
public class ConfigGroupDTO {

    @NotNull(message = "配置分组编号不能为空", groups = {Update.class})
    private Integer id;

    @NotBlank(message = "配置分组名不能为空", groups = {Insert.class, Update.class})
    @Length(max = 100, message = "配置分组名长度不允许超过100个字", groups = {Insert.class, Update.class})
    private String groupName;

    @Length(max = 200, message = "配置分组备注长度不允许超过200个字", groups = {Insert.class, Update.class})
    private String groupDesc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }
}

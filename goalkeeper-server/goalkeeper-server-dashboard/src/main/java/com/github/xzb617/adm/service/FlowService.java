package com.github.xzb617.adm.service;

import com.github.xzb617.domain.common.PageTextCondition;
import com.github.xzb617.domain.dto.FlowDTO;
import com.github.xzb617.domain.dto.FlowInfoDTO;
import com.github.xzb617.domain.entity.Flow;

import java.util.List;
import java.util.Map;

public interface FlowService {

    /**
     * 根据编号查询Flow
     * @param id 编号
     * @return
     */
    Flow getById(Integer id);

    /**
     * 根据名称查询Flow
     * @param flowName 限流规则名
     * @return
     */
    Flow getByName(String flowName);

    /**
     * 获取客户端的限流配置
     * @param flowName 限流规则名
     * @return
     */
    Map<String, Object> getClientFlowConfig(String flowName);

    /**
     * 查询限流列表
     * @param condition 查询条件
     * @return
     */
    List<Flow> getList(PageTextCondition condition);

    /**
     * 更新限流配置
     * @param dto 限流参数
     */
    void update(FlowDTO dto);

    /**
     * 创建限流规则
     * @param dto 规则参数
     */
    void saveInfo(FlowInfoDTO dto);

    /**
     * 更新限流规则
     * @param dto 规则参数
     */
    void updateInfo(FlowInfoDTO dto);

    /**
     * 删除限流规则及其所有常规限流和热点参数
     * @param id 规则编号
     */
    void deleteById(Integer id);
}

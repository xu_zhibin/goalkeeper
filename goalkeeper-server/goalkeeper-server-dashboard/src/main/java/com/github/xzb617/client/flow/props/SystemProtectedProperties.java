package com.github.xzb617.client.flow.props;

import java.util.HashMap;
import java.util.Map;

public class SystemProtectedProperties {

    private Boolean enabled = true;

    private Map<String, Integer> rules = new HashMap<>(3);

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, Integer> getRules() {
        return rules;
    }

    public void setRules(Map<String, Integer> rules) {
        this.rules = rules;
    }

}

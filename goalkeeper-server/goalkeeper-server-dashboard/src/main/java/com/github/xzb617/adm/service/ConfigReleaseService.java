package com.github.xzb617.adm.service;

import com.github.xzb617.domain.condition.ConfigReleaseCondition;
import com.github.xzb617.domain.entity.ConfigFile;
import com.github.xzb617.domain.entity.ConfigRelease;

import java.util.List;

public interface ConfigReleaseService {

    List<ConfigRelease> getList(ConfigReleaseCondition condition);

    void releaseHistory(ConfigFile configFile);

}

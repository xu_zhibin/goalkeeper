package com.github.xzb617.client.flow.props;

import org.springframework.http.HttpStatus;

public class RejectProperties {

    /**
     * 策略有：
     * 1.快速失败 QUICK_FAIL, 暂无其它
     */
    private String strategy = "QUICK_FAIL";

    /**
     * 响应状态，默认：429
     */
    private Integer responseStatus = HttpStatus.TOO_MANY_REQUESTS.value();

    /**
     * 响应内容： 默认： {"code": 429, "message": "触发限流，拒绝访问"}
     */
    private String responseContent = "Blocking by GoalKeeper";

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public Integer getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Integer responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

}

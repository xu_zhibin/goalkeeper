package com.github.xzb617.adm.service.impl;

import com.github.xzb617.adm.dao.ConfigReleaseMapper;
import com.github.xzb617.adm.service.ConfigReleaseService;
import com.github.xzb617.domain.condition.ConfigReleaseCondition;
import com.github.xzb617.domain.entity.ConfigFile;
import com.github.xzb617.domain.entity.ConfigRelease;
import org.checkerframework.checker.units.qual.C;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class ConfigReleaseServiceImpl implements ConfigReleaseService {

    private final ConfigReleaseMapper configReleaseMapper;

    public ConfigReleaseServiceImpl(ConfigReleaseMapper configReleaseMapper) {
        this.configReleaseMapper = configReleaseMapper;
    }


    @Override
    public List<ConfigRelease> getList(ConfigReleaseCondition condition) {
        return this.configReleaseMapper.selectList(condition);
    }

    @Override
    @Transactional
    public void releaseHistory(ConfigFile configFile) {
        ConfigRelease configRelease = new ConfigRelease();
        configRelease.setConfigFileId(configFile.getId());
        configRelease.setConfigGroupId(configFile.getConfigGroupId());
        configRelease.setConfigContent(configFile.getConfigContent());
        configRelease.setFileExtension(configFile.getFileExtension());
        configRelease.setRemark(configFile.getRemark());
        configRelease.setSign(configFile.getSign());
        configRelease.setReleaseTime(configFile.getReleaseTime());
        configRelease.setReleaseUser(configFile.getReleaseUser());
        this.configReleaseMapper.insertSelective(configRelease);
    }
}

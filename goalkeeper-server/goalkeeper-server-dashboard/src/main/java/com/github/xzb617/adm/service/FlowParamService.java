package com.github.xzb617.adm.service;

import com.github.xzb617.domain.entity.FlowParam;

import java.util.List;

public interface FlowParamService {
    List<FlowParam> getListByFlowId(Integer flowId);

    void update(Integer flowId, List<FlowParam> params);

    void deleteByFlowId(Integer flowId);
}

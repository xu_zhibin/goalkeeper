package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.FlowStatus;
import tk.mybatis.mapper.common.Mapper;

public interface FlowStatusMapper extends Mapper<FlowStatus> {
}
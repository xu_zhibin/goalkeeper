package com.github.xzb617.domain.common;

public class PageTextCondition extends PageCondition {

    /** 查询文本 */
    private String searchText;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public boolean isNotEmptyOrNull() {
        return this.searchText != null && !"".equalsIgnoreCase(this.searchText);
    }

    /**
     * @return "%searchText%"
     */
    public String like() {
        return "%" + this.searchText + "%";
    }

    /**
     * @return "searchText%"
     */
    public String likeLeft() {
        return this.searchText + "%";
    }

    /**
     * @return "%searchText"
     */
    public String likeRight() {
        return "%" + this.searchText;
    }

    @Override
    public String toString() {
        return "TextCondition{" +
                "searchText='" + searchText + '\'' +
                '}';
    }

}

package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_flow_metric")
public class FlowMetric {
    @Id
    private String id;

    @Column(name = "app_id")
    private String appId;

    @Column(name = "instance_id")
    private String instanceId;

    @Column(name = "total_count")
    private Long totalCount;

    @Column(name = "pass_count")
    private Long passCount;

    @Column(name = "fail_count")
    private Long failCount;

    @Column(name = "start_time")
    private Date startTime;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * @return app_id
     */
    public String getAppId() {
        return appId;
    }

    /**
     * @param appId
     */
    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    /**
     * @return instance_id
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * @param instanceId
     */
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId == null ? null : instanceId.trim();
    }

    /**
     * @return total_count
     */
    public Long getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount
     */
    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return pass_count
     */
    public Long getPassCount() {
        return passCount;
    }

    /**
     * @param passCount
     */
    public void setPassCount(Long passCount) {
        this.passCount = passCount;
    }

    /**
     * @return fail_count
     */
    public Long getFailCount() {
        return failCount;
    }

    /**
     * @param failCount
     */
    public void setFailCount(Long failCount) {
        this.failCount = failCount;
    }

    /**
     * @return start_time
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
package com.github.xzb617;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = {"com.github.xzb617.adm.dao"})
public class GoalkeeperDashboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoalkeeperDashboardApplication.class, args);
    }

}

package com.github.xzb617.domain.condition;

import com.github.xzb617.domain.common.TextCondition;

public class RegCenterInstanceCondition extends TextCondition {

    private String appId;

    private Boolean status;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}

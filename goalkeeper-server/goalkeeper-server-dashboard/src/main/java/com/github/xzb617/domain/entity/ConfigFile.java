package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_config_file")
public class ConfigFile {
    @Id
    private Integer id;

    /**
     * 所属分组编号
     */
    @Column(name = "config_group_id")
    private Integer configGroupId;

    @Column(name = "config_group_name")
    private String configGroupName;

    /**
     * 配置文件名称
     */
    @Column(name = "config_name")
    private String configName;

    /**
     * 配置文件格式，yaml/properties
     */
    @Column(name = "file_extension")
    private String fileExtension;

    /**
     * 状态：true已发布，false:未发布
     */
    private Boolean status;

    /**
     * 标签
     */
    private String sign;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者
     */
    @Column(name = "create_user")
    private String createUser;

    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改者
     */
    @Column(name = "update_user")
    private String updateUser;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "release_user")
    private String releaseUser;

    @Column(name = "release_time")
    private Date releaseTime;

    /**
     * 配置文件内容
     */
    @Column(name = "config_content")
    private String configContent;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取所属分组编号
     *
     * @return config_group_id - 所属分组编号
     */
    public Integer getConfigGroupId() {
        return configGroupId;
    }

    /**
     * 设置所属分组编号
     *
     * @param configGroupId 所属分组编号
     */
    public void setConfigGroupId(Integer configGroupId) {
        this.configGroupId = configGroupId;
    }

    /**
     * @return config_group_name
     */
    public String getConfigGroupName() {
        return configGroupName;
    }

    /**
     * @param configGroupName
     */
    public void setConfigGroupName(String configGroupName) {
        this.configGroupName = configGroupName == null ? null : configGroupName.trim();
    }

    /**
     * 获取配置文件名称
     *
     * @return config_name - 配置文件名称
     */
    public String getConfigName() {
        return configName;
    }

    /**
     * 设置配置文件名称
     *
     * @param configName 配置文件名称
     */
    public void setConfigName(String configName) {
        this.configName = configName == null ? null : configName.trim();
    }

    /**
     * 获取配置文件格式，yaml/properties
     *
     * @return file_extension - 配置文件格式，yaml/properties
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * 设置配置文件格式，yaml/properties
     *
     * @param fileExtension 配置文件格式，yaml/properties
     */
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension == null ? null : fileExtension.trim();
    }

    /**
     * 获取状态：true已发布，false:未发布
     *
     * @return status - 状态：true已发布，false:未发布
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置状态：true已发布，false:未发布
     *
     * @param status 状态：true已发布，false:未发布
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取标签
     *
     * @return sign - 标签
     */
    public String getSign() {
        return sign;
    }

    /**
     * 设置标签
     *
     * @param sign 标签
     */
    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取创建者
     *
     * @return create_user - 创建者
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建者
     *
     * @param createUser 创建者
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * @return create_time
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改者
     *
     * @return update_user - 修改者
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置修改者
     *
     * @param updateUser 修改者
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    /**
     * @return update_time
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return release_user
     */
    public String getReleaseUser() {
        return releaseUser;
    }

    /**
     * @param releaseUser
     */
    public void setReleaseUser(String releaseUser) {
        this.releaseUser = releaseUser == null ? null : releaseUser.trim();
    }

    /**
     * @return release_time
     */
    public Date getReleaseTime() {
        return releaseTime;
    }

    /**
     * @param releaseTime
     */
    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    /**
     * 获取配置文件内容
     *
     * @return config_content - 配置文件内容
     */
    public String getConfigContent() {
        return configContent;
    }

    /**
     * 设置配置文件内容
     *
     * @param configContent 配置文件内容
     */
    public void setConfigContent(String configContent) {
        this.configContent = configContent == null ? null : configContent.trim();
    }
}
package com.github.xzb617.domain.vo;

import com.github.xzb617.domain.vo.echarts.*;

import java.util.ArrayList;
import java.util.List;

/**
 * echarts的折线图数据模型
 * @author xzb617
 */
public class LineChartVO {

    private Title title;

    private Tooltip tooltip;

    private Legend legend;

    private XAxis xAxis;

    private YAxis yAxis;

    private String[] color;

    private List<Series> series = new ArrayList<>();

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public Legend getLegend() {
        return legend;
    }

    public void setLegend(Legend legend) {
        this.legend = legend;
    }

    public XAxis getxAxis() {
        return xAxis;
    }

    public void setxAxis(XAxis xAxis) {
        this.xAxis = xAxis;
    }

    public YAxis getyAxis() {
        return yAxis;
    }

    public void setyAxis(YAxis yAxis) {
        this.yAxis = yAxis;
    }

    public List<Series> getSeries() {
        return series;
    }

    public void setSeries(List<Series> series) {
        this.series = series;
    }

    public String[] getColor() {
        return color;
    }

    public void setColor(String[] color) {
        this.color = color;
    }
}

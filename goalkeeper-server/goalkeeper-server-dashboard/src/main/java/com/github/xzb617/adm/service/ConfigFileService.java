package com.github.xzb617.adm.service;

import com.github.xzb617.domain.dto.ConfigFileDTO;
import com.github.xzb617.domain.entity.ConfigFile;

import java.util.List;

public interface ConfigFileService {

    ConfigFile getById(Integer id);

    ConfigFile getByName(String name);

    ConfigFile getByClient(String configGroupName, String configFileName);

    List<ConfigFile> getList(Integer configGroupId);

    void save(ConfigFileDTO dto);

    void update(ConfigFileDTO dto);

    void release(ConfigFileDTO dto);

    void delete(Integer id);

    void deleteByGroup(Integer configGroupId);

}

package com.github.xzb617.domain.dto;

import com.github.xzb617.domain.entity.RouteRule;
import com.github.xzb617.domain.entity.RouteRuleItem;

import java.util.ArrayList;
import java.util.List;

public class RouteDTO {

    private RouteRule routeRule;

    private List<RouteRuleItem> routeRuleItems = new ArrayList<>();

    public RouteRule getRouteRule() {
        return routeRule;
    }

    public void setRouteRule(RouteRule routeRule) {
        this.routeRule = routeRule;
    }

    public List<RouteRuleItem> getRouteRuleItems() {
        return routeRuleItems;
    }

    public void setRouteRuleItems(List<RouteRuleItem> routeRuleItems) {
        this.routeRuleItems = routeRuleItems;
    }
}

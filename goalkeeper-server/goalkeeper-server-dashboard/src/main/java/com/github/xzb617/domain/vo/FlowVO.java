package com.github.xzb617.domain.vo;

import com.github.xzb617.domain.entity.Flow;
import com.github.xzb617.domain.entity.FlowParam;
import com.github.xzb617.domain.entity.FlowRule;

import java.util.ArrayList;
import java.util.List;

/**
 * 限流配置数据模型
 * @author xzb617
 */
public class FlowVO {

    private Flow flow;

    private List<FlowRule> rules = new ArrayList<>();

    private List<FlowParam> params = new ArrayList<>();

    public FlowVO() {
    }

    public FlowVO(Flow flow, List<FlowRule> rules, List<FlowParam> params) {
        this.flow = flow;
        this.rules = rules;
        this.params = params;
    }

    public Flow getFlow() {
        return flow;
    }

    public void setFlow(Flow flow) {
        this.flow = flow;
    }

    public List<FlowRule> getRules() {
        return rules;
    }

    public void setRules(List<FlowRule> rules) {
        this.rules = rules;
    }

    public List<FlowParam> getParams() {
        return params;
    }

    public void setParams(List<FlowParam> params) {
        this.params = params;
    }
}

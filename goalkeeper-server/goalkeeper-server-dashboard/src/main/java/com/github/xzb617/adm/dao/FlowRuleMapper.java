package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.FlowRule;
import tk.mybatis.mapper.common.Mapper;

public interface FlowRuleMapper extends Mapper<FlowRule> {
}
package com.github.xzb617.domain.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "gk_config_release")
public class ConfigRelease {
    @Id
    private Integer id;

    @Column(name = "config_file_id")
    private Integer configFileId;

    @Column(name = "config_file_name")
    private String configFileName;

    @Column(name = "config_group_id")
    private Integer configGroupId;

    @Column(name = "config_group_name")
    private String configGroupName;

    @Column(name = "file_extension")
    private String fileExtension;

    private String sign;

    private String remark;

    @Column(name = "release_user")
    private String releaseUser;

    @Column(name = "release_time")
    private Date releaseTime;

    @Column(name = "config_content")
    private String configContent;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return config_file_id
     */
    public Integer getConfigFileId() {
        return configFileId;
    }

    /**
     * @param configFileId
     */
    public void setConfigFileId(Integer configFileId) {
        this.configFileId = configFileId;
    }

    /**
     * @return config_file_name
     */
    public String getConfigFileName() {
        return configFileName;
    }

    /**
     * @param configFileName
     */
    public void setConfigFileName(String configFileName) {
        this.configFileName = configFileName == null ? null : configFileName.trim();
    }

    /**
     * @return config_group_id
     */
    public Integer getConfigGroupId() {
        return configGroupId;
    }

    /**
     * @param configGroupId
     */
    public void setConfigGroupId(Integer configGroupId) {
        this.configGroupId = configGroupId;
    }

    /**
     * @return config_group_name
     */
    public String getConfigGroupName() {
        return configGroupName;
    }

    /**
     * @param configGroupName
     */
    public void setConfigGroupName(String configGroupName) {
        this.configGroupName = configGroupName == null ? null : configGroupName.trim();
    }

    /**
     * @return file_extension
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * @param fileExtension
     */
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension == null ? null : fileExtension.trim();
    }

    /**
     * @return sign
     */
    public String getSign() {
        return sign;
    }

    /**
     * @param sign
     */
    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * @return release_user
     */
    public String getReleaseUser() {
        return releaseUser;
    }

    /**
     * @param releaseUser
     */
    public void setReleaseUser(String releaseUser) {
        this.releaseUser = releaseUser == null ? null : releaseUser.trim();
    }

    /**
     * @return release_time
     */
    public Date getReleaseTime() {
        return releaseTime;
    }

    /**
     * @param releaseTime
     */
    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    /**
     * @return config_content
     */
    public String getConfigContent() {
        return configContent;
    }

    /**
     * @param configContent
     */
    public void setConfigContent(String configContent) {
        this.configContent = configContent == null ? null : configContent.trim();
    }
}
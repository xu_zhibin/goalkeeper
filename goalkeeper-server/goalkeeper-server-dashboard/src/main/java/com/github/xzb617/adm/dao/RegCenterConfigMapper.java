package com.github.xzb617.adm.dao;

import com.github.xzb617.domain.entity.RegCenterConfig;
import tk.mybatis.mapper.common.Mapper;

public interface RegCenterConfigMapper extends Mapper<RegCenterConfig> {
}
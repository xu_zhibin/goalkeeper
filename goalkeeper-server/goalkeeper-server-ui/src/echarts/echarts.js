/**
 * echarts 按需引入
 * 避免引入庞大的体积
 */
// 核心模块
import * as echarts from 'echarts/core'

// 引入的图表
import { LineChart } from 'echarts/charts'

// 引入提示框、标题等通用组件
import {
    TitleComponent,
    TooltipComponent,
    GridComponent,
    LegendComponent
} from 'echarts/components'

// 引入 Canvas 渲染器 （必须）
import { SVGRenderer } from 'echarts/renderers'

// 注册
echarts.use([
    LineChart, TitleComponent, TooltipComponent, GridComponent, LegendComponent, SVGRenderer
])

export default echarts
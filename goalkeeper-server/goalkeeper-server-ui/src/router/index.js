import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  

  {
    path: '/',
    component: Layout,
    redirect: '/registry/index',
    meta: { title: '注册中心', icon: 'example'},
    children: [
      {
        path: '/registry/index',
        component: () => import('@/views/registry/index'),
        meta: { title: '应用管理'}
      },
      {
        path: '/registry/config',
        component: () => import('@/views/registry/config'),
        meta: { title: '注册配置'}
      },
      {
        path: '/registry/instances',
        component: () => import('@/views/registry/instances'),
        meta: { title: '应用实例'},
        hidden: true
      }
    ]
  },

  {
    path: '/flow',
    component: Layout,
    redirect: '/flow/monitor',
    meta: { title: '流量控制', icon: 'migrate'},
    children: [
      {
        path: '/flow/index',
        component: () => import('@/views/flow/flow'),
        meta: { title: '单机流控'}
      },
      {
        path: '/flow/monitor',
        component: () => import('@/views/flow/flow_monitor'),
        meta: { title: '流量监控'},
      },
      {
        path: '/flow/detail',
        component: () => import('@/views/flow/flow_detail'),
        meta: { title: '限流详情'},
        hidden: true
      }
    ]
  },


  {
    path: '/routes',
    component: Layout,
    redirect: '/routes/index',
    meta: { title: '路由管理', icon: 'tree'},
    children: [
      {
        path: '/routes/index',
        component: () => import('@/views/routes/index'),
        meta: { title: '路由规则'}
      },
      {
        path: '/routes/monitor',
        component: () => import('@/views/routes/route_monitor'),
        meta: { title: '路由观测'},
      },
      {
        path: '/routes/canary',
        component: () => import('@/views/routes/route_dyeing'),
        meta: { title: '流量染色'},
      },
      {
        path: '/routes/gray',
        component: () => import('@/views/routes/route_gray'),
        meta: { title: '链路灰度'},
      },
      {
        path: '/routes/transfer',
        component: () => import('@/views/routes/header_transfer'),
        meta: { title: '头参透传'},
      },
      {
        path: '/routes/detail',
        component: () => import('@/views/routes/route_detail'),
        meta: { title: '路由规则详情'},
        hidden: true
      }
    ]
  },


  {
    path: '/config',
    component: Layout,
    redirect: '/config/index',
    meta: { title: '配置中心', icon: 'scripts'},
    children: [
      {
        path: '/config/index',
        component: () => import('@/views/config/index'),
        meta: { title: '配置分组'}
      },
      {
        path: '/config/history',
        component: () => import('@/views/config/history'),
        meta: { title: '发布历史'},
      },
      {
        path: '/config/file',
        component: () => import('@/views/config/file'),
        meta: { title: '配置文件'},
        hidden: true
      }
    ]
  },

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [


  {
    path: '/system/users/profile',
    component: Layout,
    children: [
      {
        path: '/system/users/profile',
        component: () => import('@/views/system/users/profile'),
        meta: { title: '个人中心', roles: ['SA', 'CA']},
        hidden: true
      }
    ]
  },

  {
    path: '/system',
    component: Layout,
    redirect: '/system/users/index',
    meta: { title: '系统与用户', icon: 'settings', roles: ['SA']},
    children: [
      {
        path: '/system/users/user',
        component: () => import('@/views/system/users/user'),
        meta: { title: '用户管理', roles: ['SA']}
      },
      {
        path: '/system/logs/opt',
        component: () => import('@/views/system/logs/opt'),
        meta: { title: '操作日志', roles: ['SA']}
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router

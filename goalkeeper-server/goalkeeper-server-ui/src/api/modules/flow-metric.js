import axios from '@/server/gkaxios'

const FUNC_PATH = "/flowMetric"

export default {
    getMetrics
}

function getMetrics(searchForm) {
    return axios.get(FUNC_PATH+'/getClient', {params: searchForm})
}
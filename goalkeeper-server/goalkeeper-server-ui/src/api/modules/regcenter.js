import axios from '@/server/gkaxios'

const FUNC_PATH = "/regcenter"

export default {
    getApps,
    getInstances,
    modifyMetadata,
    modifyStatus
}

function getApps(searchForm) {
    return axios.get(FUNC_PATH+'/apps', {params: searchForm})
}

function getInstances(searchForm) {
    return axios.get(FUNC_PATH+'/instances', {params: searchForm})
}

function modifyMetadata(data) {
    return axios.put(FUNC_PATH+'/metadata', data)
}

function modifyStatus(data) {
    return axios.put(FUNC_PATH+'/status', data)
}
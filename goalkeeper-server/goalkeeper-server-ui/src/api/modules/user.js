import request from '@/server/gkaxios'

const FUNC_PATH = "/user"

export default {
    getById,
    getList,
    save,
    update,
    deleteById,
    resetPwd,
    modifySelf,
    modifySelfPwd
}



function getById(id) {
    return request.get(FUNC_PATH+'/getById', {
        params: {'id': id}
    })
}

function getList(searchForm) {
    return request.get(FUNC_PATH+'/getList', {params: searchForm})
}


function save(data) {
    return request.post(FUNC_PATH+'/save', data)
}

function update(data) {
    return request.put(FUNC_PATH+'/update', data)
}

function deleteById(ids) {
    return request.put(FUNC_PATH+'/deleteById', ids)
}


function resetPwd(id) {
    return request.get(FUNC_PATH+'/resetPwd', {
        params: {'id': id}
    })
}

function modifySelf(data) {
    return request.put(FUNC_PATH+'/modifySelf', data)
}

function modifySelfPwd(data) {
    return request.put(FUNC_PATH+'/modifySelfPwd', data)
}
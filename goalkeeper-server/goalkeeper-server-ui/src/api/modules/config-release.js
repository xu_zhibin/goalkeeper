import axios from '@/server/gkaxios'

const FUNC_PATH = "/config/release"

export default {
    getList
}


function getList(searchForm) {
    return axios.get(FUNC_PATH+'/getList', {params: searchForm})
}

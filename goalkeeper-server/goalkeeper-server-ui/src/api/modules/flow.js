import axios from '@/server/gkaxios'

const FUNC_PATH = "/flow"

export default {
    getById,
    getList,
    update,
    saveInfo,
    updateInfo,
    deleteById
}

function getById(flowId) {
    return axios.get(FUNC_PATH+'/getById', {params: {
        'id': flowId
    }})
}

function getList(searchForm) {
    return axios.get(FUNC_PATH+'/getList', {params: searchForm})
}

function update(data) {
    return axios.put(FUNC_PATH+'/update', data)
}

function saveInfo(data) {
    return axios.post(FUNC_PATH+'/saveInfo', data)
}

function updateInfo(data) {
    return axios.put(FUNC_PATH+'/updateInfo', data)
}

function deleteById(id) {
    return axios.delete(FUNC_PATH+'/deleteById', {
        params: {'id': id}
    })
}
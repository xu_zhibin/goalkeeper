import axios from '@/server/gkaxios'

const FUNC_PATH = "/routeFlowMetric"

export default {
    getRouteMetrics
}

function getRouteMetrics(searchForm) {
    return axios.get(FUNC_PATH+'/getRouteMetrics', {params: searchForm})
}
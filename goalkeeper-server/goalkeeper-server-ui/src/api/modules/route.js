import axios from '@/server/gkaxios'

const FUNC_PATH = "/route"

export default {
    getById,
    getList,
    update
}


function getById(flowId) {
    return axios.get(FUNC_PATH+'/getById', {params: {
        'id': flowId
    }})
}

function getList(searchForm) {
    return axios.get(FUNC_PATH+'/getList', {params: searchForm})
}

function update(data) {
    return axios.put(FUNC_PATH+'/update', data)
}
import axios from '@/server/gkaxios'

const FUNC_PATH = "/regcenter/config"

export default {
    getList,
    save,
    update,
    modifyStatus,
    deleteById
}


function getList(searchForm) {
    return axios.get(FUNC_PATH+'/getList', {params: searchForm})
}

function save(data) {
    return axios.post(FUNC_PATH+'/save', data)
}

function update(data) {
    return axios.put(FUNC_PATH+'/update', data)
}

function modifyStatus(data) {
    return axios.put(FUNC_PATH+'/modifyStatus', data)
}

function deleteById(id) {
    return axios.delete(FUNC_PATH+'/deleteById', {
        params: {'id': id}
    })
}
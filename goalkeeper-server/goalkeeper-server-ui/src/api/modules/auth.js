import axios from '@/server/gkaxios'

const FUNC_PATH = "/auth"

export default {
    login,
    logout,
    getInfo,
    getSelf
}

/**
 * 登录
 * @param data 
 */
function login(data) {
    return axios.post(FUNC_PATH+'/login', data)
}

function logout() {
    return axios.delete(FUNC_PATH+'/logout')
}

function getInfo() {
    return axios.get(FUNC_PATH+'/getInfo')
}

function getSelf() {
    return axios.get(FUNC_PATH+'/getSelf')
}
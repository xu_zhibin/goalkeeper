import axios from '@/server/gkaxios'

const FUNC_PATH = "/optlog"

export default {
    getList,
    getSelfList
}

function getList(searchForm) {
    return axios.get(FUNC_PATH+'/getList', {params: searchForm})
}

function getSelfList(searchForm) {
    return axios.get(FUNC_PATH+'/getSelfList', {params: searchForm})
}
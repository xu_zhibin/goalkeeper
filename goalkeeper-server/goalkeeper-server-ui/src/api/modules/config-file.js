import axios from '@/server/gkaxios'

const FUNC_PATH = "/config/file"

export default {
    getById,
    getList,
    save,
    update,
    release,
    deleteById
}

function getById(configId) {
    return axios.get(FUNC_PATH+'/getById', {params: {
        'id': configId
    }})
}


function getList(configGroupId) {
    return axios.get(FUNC_PATH+'/getList', {params: {
        'configGroupId': configGroupId
    }})
}

function save(data) {
    return axios.post(FUNC_PATH+'/save', data)
}

function update(data) {
    return axios.put(FUNC_PATH+'/update', data)
}

function release(data) {
    return axios.put(FUNC_PATH+'/release', data)
}

function deleteById(id) {
    return axios.delete(FUNC_PATH+'/delete', {
        params: {'id': id}
    })
}
import axios from '@/server/gkaxios'

const FUNC_PATH = "/config/group"

export default {
    getList,
    getAll,
    save,
    update,
    deleteById
}


function getList(searchForm) {
    return axios.get(FUNC_PATH+'/getList', {params: searchForm})
}

function getAll() {
    return axios.get(FUNC_PATH+'/getAll')
}

function save(data) {
    return axios.post(FUNC_PATH+'/save', data)
}

function update(data) {
    return axios.put(FUNC_PATH+'/update', data)
}

function deleteById(id) {
    return axios.delete(FUNC_PATH+'/delete', {
        params: {'id': id}
    })
}
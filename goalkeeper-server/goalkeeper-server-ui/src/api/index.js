/**
 * 后台Http接口地址统一入口
 * @author Pristine Xu 
 * @create 2021-12-16
 */
import auth from './modules/auth'
import configGroup from './modules/config-group'
import configFile from './modules/config-file'
import configRelease from './modules/config-release'
import flow from './modules/flow'
import flowMetric from './modules/flow-metric'
import route from './modules/route'
import routeFlowMetric from './modules/route-flow-metric'
import regcenter from './modules/regcenter'
import regcenterConfig from './modules/regcenter-config'
import user from './modules/user'
import optLog from './modules/opt-log'

export default {
    auth,           // 登录接口
    configGroup,    // 配置分组
    configFile,     // 配置文件
    configRelease,  // 发布历史
    flowMetric,
    flow,
    route,
    routeFlowMetric,
    regcenter,
    regcenterConfig,
    user,
    optLog
}
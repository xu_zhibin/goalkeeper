package com.github.xzb617.storage.api;

import com.github.xzb617.storage.api.dto.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "service-storage", fallbackFactory = StorageServiceFallback.class)
public interface StorageService {

    /**
     * 削减库存
     * @param uid 用户编号
     * @param count 削减数量
     * @return
     */
    @RequestMapping(value = "/reduce", method = RequestMethod.PUT)
    Result reduce(@RequestParam("uid") String uid, @RequestParam("count") Integer count);

}

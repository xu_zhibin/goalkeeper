package com.github.xzb617.storage.api;

import com.github.xzb617.storage.api.dto.Result;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class StorageServiceFallback implements FallbackFactory<StorageService> {

    @Override
    public StorageService create(Throwable throwable) {
        return new StorageService() {
            @Override
            public Result reduce(String uid, Integer count) {
                return new Result(false, "调用库存服务进行减库存操作失败", null);
            }
        };
    }

}

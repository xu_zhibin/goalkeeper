package com.github.xzb617.controller;

import com.github.xzb617.client.route.zuul.props.RouteProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class TestController {

    private final RouteProperties routeProperties;

    public TestController(RouteProperties routeProperties) {
        this.routeProperties = routeProperties;
    }

    @GetMapping("/route")
    public ResponseEntity route() {
        return ResponseEntity.status(HttpStatus.OK).body(this.routeProperties.getRule());
    }
}

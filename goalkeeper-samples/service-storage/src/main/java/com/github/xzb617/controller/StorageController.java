package com.github.xzb617.controller;

import com.github.xzb617.storage.api.StorageService;
import com.github.xzb617.storage.api.dto.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class StorageController {

    private final Logger LOGGER = LoggerFactory.getLogger(StorageController.class);

    @Value("${server.port}")
    private Integer port;
    @Value("${spring.application.name}")
    private String appName;

    @RequestMapping(value = "/reduce", method = RequestMethod.PUT)
    public Result reduce(@RequestParam("uid") String uid, @RequestParam("count") Integer count, @RequestHeader(value = "gk-header-transfer-token", required = false) String token) {
        LOGGER.info("用户： {} 成功削减库存 {}", uid, count);
        System.out.println("token: " + token);
        return new Result(true, appName+":" + port +" -> 削减库存成功", null);
    }
}

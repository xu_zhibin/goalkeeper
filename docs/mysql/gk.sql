/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80019
Source Host           : localhost:3306
Source Database       : gk

Target Server Type    : MYSQL
Target Server Version : 80019
File Encoding         : 65001

Date: 2022-12-21 17:18:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gk_config_file
-- ----------------------------
DROP TABLE IF EXISTS `gk_config_file`;
CREATE TABLE `gk_config_file` (
  `id` int NOT NULL AUTO_INCREMENT,
  `config_group_id` int NOT NULL COMMENT '所属分组编号',
  `config_group_name` varchar(100) DEFAULT NULL,
  `config_name` varchar(50) NOT NULL COMMENT '配置文件名称',
  `config_content` text COMMENT '配置文件内容',
  `file_extension` varchar(10) NOT NULL DEFAULT 'yaml' COMMENT '配置文件格式，yaml/properties',
  `status` bit(1) NOT NULL DEFAULT b'0' COMMENT '状态：true已发布，false:未发布',
  `sign` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '标签',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL,
  `update_user` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '修改者',
  `update_time` datetime DEFAULT NULL,
  `release_user` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_config_group
-- ----------------------------
DROP TABLE IF EXISTS `gk_config_group`;
CREATE TABLE `gk_config_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `group_desc` varchar(200) DEFAULT NULL,
  `config_file_count` int DEFAULT '0' COMMENT '配置文件数',
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of gk_config_group
-- ----------------------------
INSERT INTO `gk_config_group` VALUES ('2', 'DEFAULT', '默认分组', '1', '2022-12-03 21:21:14', 'goalkeeper', null, null);


-- ----------------------------
-- Table structure for gk_config_release
-- ----------------------------
DROP TABLE IF EXISTS `gk_config_release`;
CREATE TABLE `gk_config_release` (
  `id` int NOT NULL AUTO_INCREMENT,
  `config_file_id` int NOT NULL,
  `config_file_name` varchar(50) DEFAULT NULL,
  `config_group_id` int NOT NULL,
  `config_group_name` varchar(100) DEFAULT NULL,
  `config_content` text,
  `file_extension` varchar(10) DEFAULT NULL,
  `sign` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `release_user` varchar(30) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_flow
-- ----------------------------
DROP TABLE IF EXISTS `gk_flow`;
CREATE TABLE `gk_flow` (
  `id` int NOT NULL AUTO_INCREMENT,
  `flow_name` varchar(50) NOT NULL COMMENT '限流名称',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态 1：启用 0：未启用',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `sys_qps` int DEFAULT NULL COMMENT '系统保护的qps',
  `sys_cpu` int DEFAULT NULL COMMENT '系统保护的cpu载荷，0-100',
  `sys_memory` int DEFAULT NULL COMMENT '系统保护的运行内存占用率，0-100',
  `reject_strategy` varchar(50) DEFAULT 'QUICK_FAIL' COMMENT '拒绝策略',
  `reject_response_status` int DEFAULT '429' COMMENT '拒绝策略的响应状态码，默认：429',
  `reject_response_content` varchar(200) DEFAULT 'Blocking by GoalKeeper' COMMENT '拒绝策略的响应体内容，支持json，默认：Blocking by GoalKeeper',
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of gk_flow
-- ----------------------------
INSERT INTO `gk_flow` VALUES ('1', 'DEFAULT', '', null, '1002', '100', '100', 'QUICK_FAIL', '429', 'Blocking by GoalKeeper', '2022-12-04 03:31:19', 'xzb', null, null);


-- ----------------------------
-- Table structure for gk_flow_metric
-- ----------------------------
DROP TABLE IF EXISTS `gk_flow_metric`;
CREATE TABLE `gk_flow_metric` (
  `id` varchar(64) NOT NULL,
  `app_id` varchar(50) NOT NULL,
  `instance_id` varchar(50) NOT NULL,
  `total_count` bigint DEFAULT NULL,
  `pass_count` bigint DEFAULT NULL,
  `fail_count` bigint DEFAULT NULL,
  `start_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_flow_param
-- ----------------------------
DROP TABLE IF EXISTS `gk_flow_param`;
CREATE TABLE `gk_flow_param` (
  `id` int NOT NULL AUTO_INCREMENT,
  `flow_id` int NOT NULL,
  `method_url` varchar(255) DEFAULT NULL,
  `param_name` varchar(50) DEFAULT NULL,
  `param_type` varchar(20) DEFAULT NULL,
  `param_value` varchar(50) DEFAULT NULL,
  `qps` int DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_flow_rule
-- ----------------------------
DROP TABLE IF EXISTS `gk_flow_rule`;
CREATE TABLE `gk_flow_rule` (
  `id` int NOT NULL AUTO_INCREMENT,
  `flow_id` int NOT NULL,
  `rule_type` varchar(20) DEFAULT NULL,
  `rule_key` varchar(255) DEFAULT NULL,
  `rule_qps` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_flow_status
-- ----------------------------
DROP TABLE IF EXISTS `gk_flow_status`;
CREATE TABLE `gk_flow_status` (
  `flow_name` varchar(50) NOT NULL,
  `changed_time` datetime DEFAULT NULL COMMENT '配置更新时间',
  PRIMARY KEY (`flow_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_opt_log
-- ----------------------------
DROP TABLE IF EXISTS `gk_opt_log`;
CREATE TABLE `gk_opt_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `request_uri` varchar(150) DEFAULT NULL,
  `opt_method` varchar(255) DEFAULT NULL,
  `opt_desc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `opt_args` varchar(2048) DEFAULT NULL,
  `opt_time` datetime DEFAULT NULL,
  `opt_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_reg_center_config
-- ----------------------------
DROP TABLE IF EXISTS `gk_reg_center_config`;
CREATE TABLE `gk_reg_center_config` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reg_center_name` varchar(50) NOT NULL,
  `reg_center_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'EUREKA' COMMENT '注册中心类型，目前仅支持EUREKA',
  `reg_center_addr` varchar(255) NOT NULL COMMENT '服务地址',
  `status` bit(1) NOT NULL COMMENT '是否',
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_route_rule
-- ----------------------------
DROP TABLE IF EXISTS `gk_route_rule`;
CREATE TABLE `gk_route_rule` (
  `id` int NOT NULL AUTO_INCREMENT,
  `route_rule_name` varchar(100) NOT NULL COMMENT '路由规则名称，要求唯一',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否启用',
  `remark` varchar(255) DEFAULT NULL,
  `sign` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '每一次创建或更新都会生成一条新的签名',
  `create_time` datetime DEFAULT NULL,
  `creata_user` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_route_rule_item
-- ----------------------------
DROP TABLE IF EXISTS `gk_route_rule_item`;
CREATE TABLE `gk_route_rule_item` (
  `id` int NOT NULL AUTO_INCREMENT,
  `route_rule_id` int NOT NULL,
  `rule_type` varchar(50) NOT NULL,
  `rule_key` varchar(100) NOT NULL,
  `caller_app_id` varchar(100) NOT NULL DEFAULT '*',
  `caller_key` varchar(64) DEFAULT NULL,
  `caller_value` varchar(64) DEFAULT NULL,
  `callee_app_id` varchar(100) NOT NULL DEFAULT '*',
  `callee_label` varchar(64) DEFAULT NULL,
  `callee_label_value` varchar(64) DEFAULT NULL,
  `weight` int NOT NULL DEFAULT '100' COMMENT '权重，1-100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for gk_users
-- ----------------------------
DROP TABLE IF EXISTS `gk_users`;
CREATE TABLE `gk_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `salt` varchar(10) DEFAULT NULL,
  `realname` varchar(50) NOT NULL,
  `phone_number` varchar(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `role` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'CA' COMMENT '角色，SA:超管，CA:普管',
  `create_time` datetime NOT NULL,
  `create_user` varchar(30) NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of gk_users
-- ----------------------------
INSERT INTO `gk_users` VALUES ('1', 'goalkeeper', '2575fff035c88f362dc47acf0125bada0e293182', '6886', 'sa', '18559935461', '18559935466@163.com', '研发部', 'SA', '2022-12-02 14:11:34', 'SYS_DEFAULT', '2022-12-20 15:35:09', 'goalkeeper');
INSERT INTO `gk_users` VALUES ('2', 'test', '556e2dc15f6616279cbea4b596bf97750deeff64', '0c7123', 'test', '18359136969', '18359136969@qq.com', '研发部', 'CA', '2022-12-19 01:12:24', 'goalkeeper', null, null);

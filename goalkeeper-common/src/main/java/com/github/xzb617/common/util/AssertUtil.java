package com.github.xzb617.common.util;

public class AssertUtil {

    /**
     * 不为 NULL
     * @param var
     * @param errMsg
     */
    public static void notNull(Object var, String errMsg) {
        if (var == null) {
            throw new IllegalArgumentException(errMsg);
        }
    }

}

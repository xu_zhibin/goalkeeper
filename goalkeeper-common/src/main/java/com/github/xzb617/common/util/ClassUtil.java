package com.github.xzb617.common.util;

import java.lang.reflect.Constructor;

/**
 * 类工具类
 * @author xzb617
 */
public class ClassUtil {

    /**
     * 实例化对象
     * @param clazzRef
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> T getInstance(String clazzRef) throws Exception {
        Class<?> algClazz = Class.forName(clazzRef);
        Constructor<?> constructor = algClazz.getDeclaredConstructor();
        return (T) constructor.newInstance();
    }

    /**
     * 实例化对象
     * @param algClazz
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> T getInstance(Class<?> algClazz) throws Exception {
        Constructor<?> constructor = algClazz.getDeclaredConstructor();
        return (T) constructor.newInstance();
    }


}

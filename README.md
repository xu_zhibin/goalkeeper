# goalkeeper

#### 介绍
goalkeeper致力于解决SpringCloudNetflix微服务的综合治理，客户端通过增加不同功能模块，实现了对应用的管理、流量管控监测、路由管理、灰度路由、配置中心等功能，是一个一站化治理平台。

#### 软件架构
![项目架构图](docs/images/goalkeeper.png "项目架构图")


#### 安装教程

1.  从gitee或github上拉取源码到本地
2.  提供一个mysql环境版本>8，执行docs/mysql下的gk.sql的建表脚本
3.  修改goalkeeper-server下的dashboard的配置文件的中数据量链接账号密码等参数（上一步配置的参数），编译打包并启动
4.  编译打包goalkeeper-sdk下的所有sdk包，并安装到本地maven仓库或远程私有仓库
5.  客户端按需引入sdk，并配置链接dashboard的地址、令牌，即可获得该模块的能力

#### 客户端SDK
所有模块都需要依赖于goalkeeper-client-bootstrap

|  SDK模块   | 说明  |
|  ----  | ----  |
| goalkeeper-client-config-sdk  | 配置中心模块 |
| goalkeeper-client-flow-sdk  | 限流模块 |
| goalkeeper-client-flow-sdk-webflux-adapter  | 限流模块WebFlux适配包 |
| goalkeeper-client-route-feign-sdk  | 非网关的Feign路由模块 |
| goalkeeper-client-route-dubbo3-sdk  | 非网关的Dubbo3路由模块（该模块后续会上传） |
| goalkeeper-client-route-grpc-sdk  | 非网关的GRPC路由模块（该模块后续会上传） |
| goalkeeper-client-route-zuul-sdk  | Zuul网关路由模块 |
| goalkeeper-client-route-scg-sdk  | SpringCloudGateway网关路由模块 |


#### 使用事项

1.  如果Dashboard配置了访问令牌和秘钥，则客户端也需要同样配置令牌和秘钥
2.  goalkeeper的注册中心模块依赖于eureka服务，需要提供eureka服务，如果是其他类型注册中心，则可以忽略该模块
3.  自定义的路由规则不影响原有应用实例的区域化部署，比如eureka的zone
4.  flow sdk、route sdk依赖于config sdk, 即需要同时引入 config sdk

#### 配置示例
##### 客户端SDK配置
- 基础配置
~~~yaml
goalkeeper:
  client:
    server-addrs: http://127.0.0.1:10500/goalkeeper
~~~
- config配置
~~~yaml
goalkeeper:
  config:
    name: 'DEFAULT'
    file-extension: 'yaml'
    group: 'DEFAULT_GROUP'
    refresh-enabled: true
~~~
- flow配置
~~~yaml
goalkeeper:
  flow:
    name: 'DEFAULT_FLOW'
      rules:
        enabled: true
        limits:
          IP:
            127.0.0.1: 10
~~~
- zuul route配置
~~~yaml
goalkeeper:
  route:
    enabled: true
    rule:
      rules:
        IP:
          "[127.0.0.1]":
            callerAppId: "*"  # 调用方服务，* 所有服务
            calleeAppId: "service-u"  # 被用方服务，* 所有服务
            callerKey: "127.0.0.1"
            callerValue: ""
            calleeLabel: "gk-route-rule"
            calleeLabelValue: "v0.2"
            weight: 100
~~~
##### 服务端配置
~~~yaml
# 项目配置
goalkeeper:
  security:
    token-header: 'Authorization'
    token-secret: '12345678ABCDEFGH'
    client-access-key: 'xxxxxxxxxxxxxxx'
    client-access-secret: 'xxxxxxxxxxxxxx'
~~~

#### 最佳实践
- 虽然限流模块可以配置在任一客户端中，但是为了统一方便的管理限流，推荐将限流模块配置在网关集群中即可
- 路由模块支持全链路和单链路，一般来说如果功能重叠不见重复配置多个路由子项，可以在一项完成之后在重新配置一项

#### 功能界面
![功能界面](docs/images/func01.png "功能界面")
![功能界面](docs/images/func02.png "功能界面")
![功能界面](docs/images/func03.png "功能界面")
![功能界面](docs/images/func04.png "功能界面")
![功能界面](docs/images/func05.png "功能界面")
![功能界面](docs/images/func06.png "功能界面")
![功能界面](docs/images/func07.png "功能界面")

#### 参与贡献

1.  非常欢迎您来参与共建该项目，如果有什么建议和意见也欢迎提出。
2.  您可以 Fork 本仓库、新建 Feat_xxx 分支、提交代码、新建 Pull Request来参与本项目


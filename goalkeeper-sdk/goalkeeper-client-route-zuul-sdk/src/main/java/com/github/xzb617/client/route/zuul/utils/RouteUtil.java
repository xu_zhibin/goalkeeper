package com.github.xzb617.client.route.zuul.utils;

import com.github.xzb617.client.route.zuul.constant.RouteRule;
import com.github.xzb617.common.constants.Bool;
import com.netflix.zuul.context.RequestContext;
import io.jmnarloch.spring.cloud.ribbon.api.RibbonFilterContext;
import io.jmnarloch.spring.cloud.ribbon.support.RibbonFilterContextHolder;

/**
 * Route Util
 * @author xzb617
 */
public class RouteUtil {

    /**
     * Assign route label
     * @param routeRule route rule
     * @param equal value is equal or no equal
     */
    public static void assignRouteLabel(RouteRule routeRule, boolean equal) {
        String key = routeRule.getCalleeLabel();
        String value = routeRule.getCalleeLabelValue();
        // Assign request header route label
        // gk-route-rule --- gk-route-version
        RequestContext zuulCtx = RequestContext.getCurrentContext();
        zuulCtx.addZuulRequestHeader(key, value);
        zuulCtx.addZuulRequestHeader(key + "-caller", routeRule.getCallerAppId());
        zuulCtx.addZuulRequestHeader(key + "-callee", routeRule.getCalleeAppId());
        zuulCtx.addZuulRequestHeader(key + "-equal", equal?Bool.TRUE:Bool.FALSE);

        // Assign ribbon context route label
        RibbonFilterContext ribbonCtx = RibbonFilterContextHolder.getCurrentContext();
        ribbonCtx.add(key, value);
        ribbonCtx.add(key + "-caller", routeRule.getCallerAppId());
        ribbonCtx.add(key + "-callee", routeRule.getCalleeAppId());
        ribbonCtx.add(key + "-equal", equal?Bool.TRUE:Bool.FALSE);
    }

}

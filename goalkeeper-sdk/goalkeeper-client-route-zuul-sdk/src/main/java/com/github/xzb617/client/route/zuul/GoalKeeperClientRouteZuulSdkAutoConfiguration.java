package com.github.xzb617.client.route.zuul;

import com.github.xzb617.client.route.zuul.ribbon.GoalkeeperRoundRobinRule;
import com.netflix.niws.loadbalancer.DiscoveryEnabledNIWSServerList;
import io.jmnarloch.spring.cloud.ribbon.rule.DiscoveryEnabledRule;
import io.jmnarloch.spring.cloud.ribbon.support.RibbonDiscoveryRuleAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ConditionalOnClass(DiscoveryEnabledNIWSServerList.class)
@AutoConfigureBefore({RibbonClientConfiguration.class, RibbonDiscoveryRuleAutoConfiguration.class})
public class GoalKeeperClientRouteZuulSdkAutoConfiguration {

    @Bean
    @Scope("prototype")
    public DiscoveryEnabledRule goalkeeperMetadataBalancerRule() {
        return new GoalkeeperRoundRobinRule();
    }

}

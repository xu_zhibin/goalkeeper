package com.github.xzb617.client.route.zuul.rules.impl;

import com.github.xzb617.client.route.zuul.constant.RouteRule;
import com.github.xzb617.client.route.zuul.props.RouteProperties;
import com.github.xzb617.client.route.zuul.rules.RoutingRule;
import com.github.xzb617.client.route.zuul.rules.ratio.WeightCalculator;
import com.github.xzb617.client.route.zuul.utils.RouteUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;

public class HeaderRoutingRule implements RoutingRule {

    @Override
    public void matches(HttpServletRequest request, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain) {
        Map<String, Map<String, RouteRule>> rules = config.getRules();
        Map<String, RouteRule> headerRuleMap = rules.get("HEADER");
        if (headerRuleMap!=null && !headerRuleMap.isEmpty()) {
            RouteRule matchedRouteRule = this.matchHeader(request, headerRuleMap);
            if (matchedRouteRule != null) {
                boolean available = WeightCalculator.available(ipAddr, matchedRouteRule.getWeight());
                if (available) {
                    // 匹配成功，打上标签
                    RouteUtil.assignRouteLabel(matchedRouteRule, true);
                } else {
                    RouteUtil.assignRouteLabel(matchedRouteRule, false);
                }
            }
        }
    }

    private RouteRule matchHeader(HttpServletRequest request, Map<String, RouteRule> headerRuleMap) {
        Enumeration<String> headerNames = request.getHeaderNames();
        RouteRule matchedRouteRule = null;
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if (headerRuleMap.containsKey(headerName)) {
                RouteRule routeRule = headerRuleMap.get(headerName);
                String headerValue = request.getHeader(headerName);
                String callerValue = routeRule.getCallerValue();
                if (callerValue!=null && callerValue.equals(headerValue)) {
                    matchedRouteRule = routeRule;
                    break;
                }
            }
        }
        return matchedRouteRule;
    }

}

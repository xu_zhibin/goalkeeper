package com.github.xzb617.client.route.zuul.rules.impl;

import com.github.xzb617.client.route.zuul.constant.RouteRule;
import com.github.xzb617.client.route.zuul.props.RouteProperties;
import com.github.xzb617.client.route.zuul.rules.RoutingRule;
import com.github.xzb617.client.route.zuul.rules.ratio.WeightCalculator;
import com.github.xzb617.client.route.zuul.utils.RouteUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;

/**
 * 支持Get类型的查询参数匹配规则
 * @author xzb617
 */
public class QueryParamRoutingRule implements RoutingRule {

    @Override
    public void matches(HttpServletRequest request, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain) {
        Map<String, Map<String, RouteRule>> rules = config.getRules();
        Map<String, RouteRule> queryParamRuleMap = rules.get("QUERY");
        if (queryParamRuleMap!=null && !queryParamRuleMap.isEmpty()) {
            RouteRule matchedRouteRule = this.matchQueryParam(request, queryParamRuleMap);
            if (matchedRouteRule != null) {
                boolean available = WeightCalculator.available(ipAddr, matchedRouteRule.getWeight());
                if (available) {
                    // 匹配成功，打上标签
                    RouteUtil.assignRouteLabel(matchedRouteRule, true);
                } else {
                    RouteUtil.assignRouteLabel(matchedRouteRule, false);
                }
            }
        }
    }

    /**
     * 匹配参数的key和value
     * @param request
     * @param queryParamRuleMap
     * @return
     */
    private RouteRule matchQueryParam(HttpServletRequest request, Map<String, RouteRule> queryParamRuleMap) {
        Enumeration<String> parameterNames = request.getParameterNames();
        RouteRule matchedRouteRule = null;
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            if (queryParamRuleMap.containsKey(paramName)) {
                RouteRule routeRule = queryParamRuleMap.get(paramName);
                String callerValue = routeRule.getCallerValue();
                String paramValue = request.getParameter(paramName);
                if (callerValue!=null && callerValue.equals(paramValue)) {
                    matchedRouteRule = routeRule;
                    break;
                }
            }
        }
        return matchedRouteRule;
    }
}

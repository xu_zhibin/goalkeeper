package com.github.xzb617.client.route.zuul.rules.ratio;

import com.github.xzb617.client.route.zuul.utils.IpHashUtil;

/**
 * 比重计算器
 * @author xzb617
 */
public class WeightCalculator {

    /**
     * 判断该规则所在IP是否在指定比重范围内
     * @param ipAddr ip
     * @param ratio 比重
     * @return boolean
     */
    public static boolean available(String ipAddr, Integer ratio) {
        int varRatio = hashIpAddr(ipAddr);
        return varRatio <= ratio;
    }

    /**
     * 对IP地址进行Hash取模运算
     * 示例： 127.0.0.1 = 5
     * @param ipAddr IP地址
     * @return 0-100
     */
    private static int hashIpAddr(String ipAddr) {
        int ipHash = IpHashUtil.hash(ipAddr);
        return ipHash % 100;
    }

}

package com.github.xzb617.client.route.zuul.rules;

import com.github.xzb617.client.route.zuul.props.RouteProperties;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class RoutingRuleChain implements RoutingRule {

    private final List<RoutingRule> rules;
    private final int size;
    private int position;

    public RoutingRuleChain(List<RoutingRule> rules) {
        this.rules = rules;
        this.size = rules.size();
        this.position = 0;
    }

    @Override
    public boolean enabled() {
        return true;
    }

    @Override
    public void executeMatches(HttpServletRequest request, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain) {
        if (this.position != this.size) {
            // 调用过滤链中过滤器的执行方法
            ++ this.position;
            RoutingRule nextRule = this.rules.get(this.position - 1);
            nextRule.executeMatches(request, ipAddr, config, this);
        }
    }

    @Override
    public void matches(HttpServletRequest request, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain) {
        // nothing need to do ...
    }
}

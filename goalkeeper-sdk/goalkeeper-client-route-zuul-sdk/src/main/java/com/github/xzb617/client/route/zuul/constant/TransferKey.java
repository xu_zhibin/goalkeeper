package com.github.xzb617.client.route.zuul.constant;

/**
 * 头参透传的Key
 * @author xzb617
 */
public class TransferKey {

    /**
     * 需要进行透传的请求头参数都必须以 HEADER_TRANSFER_PREFIX 开头
     * 示例：需要在整个链路中传递请求头 token
     *      只需要在客户端请求的时候携带参数 gk-header-transfer-token=${token_value}
     */
    public final static String HEADER_TRANSFER_PREFIX = "gk-header-transfer-";

}

package com.github.xzb617.client.route.zuul.constant;

/**
 * 路由的Key
 * @author xzb
 */
public class RouteKey {

    /**
     * 规则式路由统一前缀
     * 1.示例： 比如设置一个路由标签为 env = test, 就可以设置成 <code>gk-route-env=test</code>
     * 2.固定值： 默认有以下固定的值用来表示一类路由标签
     *  （1）gk-route-version=v1.0： 版本标签，可支持金丝雀路由和蓝绿路由
     *  （3）其它的均标签均为自定义的规则式路由
     *  3.路由标签的传递（透传）
     *  （1）客户端携带
     *  （2）网关按规则携带
     */
    public final static String ROUTE_KEY_PREFIX = "gk-route-";

    /**
     * 自定义规则标签头
     */
    public final static String ROUTE_RULE_KEY = ROUTE_KEY_PREFIX + "rule";

}

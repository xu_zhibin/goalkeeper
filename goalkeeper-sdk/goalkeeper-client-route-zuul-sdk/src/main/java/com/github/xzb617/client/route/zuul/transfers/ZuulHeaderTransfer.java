package com.github.xzb617.client.route.zuul.transfers;

import com.github.xzb617.client.route.zuul.constant.TransferKey;
import com.netflix.zuul.context.RequestContext;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 头参透传
 * @author xzb617
 */
public class ZuulHeaderTransfer {

    /**
     * 匹配所有需要透传的请求头
     * @param request 请求
     * @return
     */
    public static void matchNeedTransferHeadersAndAddToZuulHeader(HttpServletRequest request) {
        Enumeration<String> headerNames = request.getHeaderNames();
        RequestContext zuulCtx = RequestContext.getCurrentContext();
        while (headerNames.hasMoreElements()) {
            String hname = headerNames.nextElement();
            if (hname!=null && hname.startsWith(TransferKey.HEADER_TRANSFER_PREFIX)) {
                String hvalue = request.getHeader(hname);
                zuulCtx.addZuulRequestHeader(hname, hvalue);
            }
        }
    }

}

package com.github.xzb617.client.route.zuul.rules;

import com.github.xzb617.client.route.zuul.props.RouteProperties;

import javax.servlet.http.HttpServletRequest;

public interface RoutingRule {

    default boolean enabled() { return true; }

    default void executeMatches(HttpServletRequest request, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain) {
        // 是否使用本规则
        if (this.enabled()) {
            this.matches(request, ipAddr, config, routingRuleChain);
        }
        // 执行下一个规则
        routingRuleChain.executeMatches(request, ipAddr, config, routingRuleChain);
    }

    public void matches(HttpServletRequest request, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain);

}

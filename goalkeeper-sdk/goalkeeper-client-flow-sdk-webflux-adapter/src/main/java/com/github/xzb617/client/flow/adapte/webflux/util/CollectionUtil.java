package com.github.xzb617.client.flow.adapte.webflux.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 集合工具类
 */
public class CollectionUtil {

    /**
     * 字符串数组转字符串集合
     * @param arr 字符串数组
     * @return
     */
    public static List<String> toList(String[] arr) {
        return Stream.of(arr).collect(Collectors.toList());
    }

    /**
     * 字符串集合转字符串数组
     * @param list 字符串集合
     * @return
     */
    public static String[] toArray(List<String> list) {
        if (list == null) {
            throw new IllegalArgumentException("argument list cannot be null.");
        }
        return list.toArray(new String[list.size()]);
    }

}

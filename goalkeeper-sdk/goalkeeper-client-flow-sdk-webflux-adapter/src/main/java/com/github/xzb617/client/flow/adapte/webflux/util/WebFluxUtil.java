package com.github.xzb617.client.flow.adapte.webflux.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.net.InetSocketAddress;

public class WebFluxUtil {

    private final static String IP_UNKNOWN = "unknown";


    /**
     * 获取域名
     * @return
     */
    public static String getDomainName(String url) {
        String result = "";
        int j = 0, startIndex = 0, endIndex = 0;
        for (int i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '/') {
                j++;
                if (j == 2)
                    startIndex = i;
                else if (j == 3)
                    endIndex = i;
            }

        }
        result = url.substring(startIndex + 1, endIndex);
        return result;
    }


    /**
     * 获取客户端IP地址
     * @param request
     * @return
     */
    public static String getClientIpAddr(ServerHttpRequest request) {
        HttpHeaders headers = request.getHeaders();
        String ip = headers.getFirst("x-forwarded-for");
        if (ip == null || ip.length() == 0 || IP_UNKNOWN.equalsIgnoreCase(ip)) {
            ip = headers.getFirst("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || IP_UNKNOWN.equalsIgnoreCase(ip)) {
            ip = headers.getFirst("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || IP_UNKNOWN.equalsIgnoreCase(ip)) {
            ip = headers.getFirst("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || IP_UNKNOWN.equalsIgnoreCase(ip)) {
            ip = headers.getFirst("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || IP_UNKNOWN.equalsIgnoreCase(ip)) {
            InetSocketAddress remoteAddress = request.getRemoteAddress();
            if (remoteAddress != null) {
                ip = remoteAddress.getAddress().getHostAddress();
            }
        }
        // 多重代理，则取第一个ip
        if (ip!=null && ip.contains(",")) {
            return ip.split(",")[0];
        }
        // 本地ip
        if (isLocalIp(ip)) {
            return "127.0.0.1";
        }
        return ip;
    }

    /**
     * 是否为本地IP地址
     * @param ip
     * @return
     */
    private static boolean isLocalIp(String ip) {
        return "0:0:0:0:0:0:0:1".equals(ip);
    }


}

package com.github.xzb617.client.flow.adapte.webflux.util;

import org.springframework.util.MultiValueMap;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class QueryParamMapUtil {

    public static Map<String, String[]> toMapOfArray(MultiValueMap<String, String> map) {
        if (map == null) {
            throw new IllegalArgumentException("argument cannot be null.");
        }
        Set<Map.Entry<String, List<String>>> entries = map.entrySet();
        Map<String, String[]> resMap = new LinkedHashMap<>(entries.size());
        for (Map.Entry<String, List<String>> entry : entries) {
            resMap.put(entry.getKey(), CollectionUtil.toArray(entry.getValue()));
        }
        return resMap;
    }

}

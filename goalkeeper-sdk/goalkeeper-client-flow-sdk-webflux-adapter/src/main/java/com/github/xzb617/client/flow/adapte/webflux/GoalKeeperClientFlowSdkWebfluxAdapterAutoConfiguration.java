package com.github.xzb617.client.flow.adapte.webflux;

import com.github.xzb617.client.flow.adapte.webflux.filter.FlowControlWebfluxFilter;
import com.github.xzb617.client.flow.cache.CounterCache;
import com.github.xzb617.client.flow.core.FlowProperties;
import com.github.xzb617.client.flow.fallback.FlowFallback;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoalKeeperClientFlowSdkWebfluxAdapterAutoConfiguration {

    @Bean
    public FlowControlWebfluxFilter flowControlWebfluxFilter(FlowProperties flowProperties, CounterCache counterCache, FlowFallback flowFallback) {
        return new FlowControlWebfluxFilter(flowProperties, counterCache, flowFallback);
    }

}

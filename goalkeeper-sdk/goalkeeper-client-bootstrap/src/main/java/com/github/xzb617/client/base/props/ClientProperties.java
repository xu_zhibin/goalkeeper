package com.github.xzb617.client.base.props;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = ClientProperties.PREFIX)
public class ClientProperties {

    public final static String PREFIX = "goalkeeper.client";

    /**
     * 服务端地址，默认地址为： http://127.0.0.1:10500, 多个地址用逗号隔开
     */
    private String serverAddrs = "http://127.0.0.1:10500/goalkeeper";

    /**
     * 配置在服务端区分同个集群不同实例之间唯一标识
     */
    @Value("${server.port}")
    private String instanceId;

    /**
     * 访问服务端的Key
     */
    private String accessKey;

    /**
     * 访问服务端的秘钥
     */
    private String accessSecret;

    /**
     * 负载均衡策略
     */
    private String loadBalanceStrategy = "com.github.xzb617.rpc.loadbalancer.strategy.RoundRobinLoadBalancer";


    public String getServerAddrs() {
        return serverAddrs;
    }

    public void setServerAddrs(String serverAddrs) {
        this.serverAddrs = serverAddrs;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getAccessSecret() {
        return accessSecret;
    }

    public void setAccessSecret(String accessSecret) {
        this.accessSecret = accessSecret;
    }

    public String getLoadBalanceStrategy() {
        return loadBalanceStrategy;
    }

    public void setLoadBalanceStrategy(String loadBalanceStrategy) {
        this.loadBalanceStrategy = loadBalanceStrategy;
    }
}

package com.github.xzb617.client.base.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@RefreshScope
@Component
@ConfigurationProperties(prefix = "goalkeeper")
public class BaseProperties {

    private FlowProperties flow = new FlowProperties();

    private RouteProperties route = new RouteProperties();

    /**
     * 流控预设配置
     * <p>
     *     方便与配置中心结合使用
     * </p>
     * @author xzb617
     */
    public class FlowProperties {

        /**
         * 远程流控配置名，只有当有远程配置生效时，本配置有效，默认：DEFAULT_FLOW
         */
        private String name = "DEFAULT";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class RouteProperties {

        private String name = "DEFAULT";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public FlowProperties getFlow() {
        return flow;
    }

    public void setFlow(FlowProperties flow) {
        this.flow = flow;
    }

    public RouteProperties getRoute() {
        return route;
    }

    public void setRoute(RouteProperties route) {
        this.route = route;
    }
}

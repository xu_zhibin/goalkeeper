package com.github.xzb617.client.flow.core.rules.chain;

import com.github.xzb617.client.flow.cache.CounterCache;
import com.github.xzb617.client.flow.core.FlowContext;
import com.github.xzb617.client.flow.core.FlowException;
import com.github.xzb617.client.flow.core.FlowProperties;
import javax.servlet.http.HttpServletRequest;

/**
 * 规则限流器的规则,一个规则提供一个校验逻辑
 * @author xzb617
 */
public interface Rule {

    default boolean enabled(FlowProperties.RulesProperties config) {return true;}

    default void validate(FlowContext context, FlowProperties.RulesProperties config, CounterCache cache, RuleChain chain) throws FlowException {
        // 是否使用本规则
        if (this.enabled(config)) {
            this.validateInternal(context, config, cache, chain);
        }
        // 下一个规则
        chain.validate(context, config, cache, chain);
    }

    public void validateInternal(FlowContext context, FlowProperties.RulesProperties config, CounterCache cache, RuleChain chain) throws FlowException;

}

package com.github.xzb617.client.flow.cache;


import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

/**
 * 基于GuavaCache的本地缓存实现
 * @author xzb617
 */
public class LocalCounterCache implements CounterCache {

    /**
     * Caffeine本地缓存
     */
    private final static LoadingCache<String, LongAdder> CACHE = Caffeine.newBuilder()
            .initialCapacity(5000)
            .maximumSize(200000)
            // 写入1s后失效
            .expireAfterWrite(1, TimeUnit.SECONDS)
            .build(new CacheLoader<String, LongAdder>() {
                @Nullable
                @Override
                public LongAdder load(@NonNull String s) throws Exception {
                    return new LongAdder();
                }
            });


    @Override
    public long incrementAndGet(String key) {
        LongAdder longAdder = CACHE.get(key);
        if (longAdder == null) {
            return 1;
        }
        longAdder.increment();
        CACHE.put(key, longAdder);
        return longAdder.longValue();
    }

}

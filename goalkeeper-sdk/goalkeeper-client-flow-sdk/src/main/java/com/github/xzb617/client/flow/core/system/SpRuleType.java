package com.github.xzb617.client.flow.core.system;

/**
 * 系统保护规则类型
 * @author xzb617
 */
public class SpRuleType {

    public final static String QPS = "qps";

    public final static String CPU = "cpu";

    public final static String MEMORY = "memory";

}

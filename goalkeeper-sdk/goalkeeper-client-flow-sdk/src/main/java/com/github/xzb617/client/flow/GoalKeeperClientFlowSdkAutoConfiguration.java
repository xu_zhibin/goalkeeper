package com.github.xzb617.client.flow;

import com.github.xzb617.client.flow.adapte.boot.BootFlowFallback;
import com.github.xzb617.client.flow.adapte.boot.FlowControlBootFilter;
import com.github.xzb617.client.flow.cache.CounterCache;
import com.github.xzb617.client.flow.cache.LocalCounterCache;
import com.github.xzb617.client.flow.core.FlowProperties;
import com.github.xzb617.client.flow.fallback.FlowFallback;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoalKeeperClientFlowSdkAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(CounterCache.class)
    public CounterCache counterCache() {
        return new LocalCounterCache();
    }

    /**
     *spring 环境
     * @param flowProperties
     * @return
     */
    @Bean
    public FlowFallback bootFlowFallback(FlowProperties flowProperties) {
        return new BootFlowFallback(flowProperties);
    }

    @Bean
    public FlowControlBootFilter flowControlBootFilter(FlowProperties flowProperties, CounterCache counterCache, FlowFallback flowFallback) {
        return new FlowControlBootFilter(flowProperties, counterCache, flowFallback);
    }

}

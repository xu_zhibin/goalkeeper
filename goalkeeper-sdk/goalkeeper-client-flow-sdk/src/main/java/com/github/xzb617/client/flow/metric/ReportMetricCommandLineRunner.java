package com.github.xzb617.client.flow.metric;

import com.github.xzb617.common.executors.GoalKeeperExecutors;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class ReportMetricCommandLineRunner implements CommandLineRunner {

    private final MetricService metricService;

    private final ScheduledExecutorService reportMetricService = GoalKeeperExecutors.newSingleThreadScheduledExecutor("report-flow-metric", true);

    public ReportMetricCommandLineRunner(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public void run(String... args) throws Exception {
        // 每60s上报一次指标参数
        ReportMetricRunnable reportMetricRunnable = new ReportMetricRunnable(this.metricService);
        this.reportMetricService.scheduleAtFixedRate(reportMetricRunnable, 10000, 60000, TimeUnit.MILLISECONDS);
    }

}

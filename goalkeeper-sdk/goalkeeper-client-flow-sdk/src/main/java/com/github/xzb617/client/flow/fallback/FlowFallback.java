package com.github.xzb617.client.flow.fallback;

import com.github.xzb617.client.flow.core.FlowException;

import javax.servlet.http.HttpServletResponse;

/**
 * 限流的降级策略
 */
public interface FlowFallback {

    FlowResponse fallback(FlowException e);

}

package com.github.xzb617.client.flow.core;

/**
 * 限流异常
 * <p>
 *     被限流时抛出
 * </p>
 * @author xzb617
 */
public class FlowException extends RuntimeException {

    public FlowException(String message) {
        super(message);
    }

    public FlowException(String message, Throwable cause) {
        super(message, cause);
    }
}

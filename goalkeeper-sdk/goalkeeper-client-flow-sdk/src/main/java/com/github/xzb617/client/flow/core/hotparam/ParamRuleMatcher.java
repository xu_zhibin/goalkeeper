package com.github.xzb617.client.flow.core.hotparam;

import java.util.Map;
import java.util.Set;

public class ParamRuleMatcher {

    public static boolean matched(Map<String, String[]> parameterMap, Map<String, Object> rule) {
        boolean isMatched = false;
        Set<Map.Entry<String, String[]>> entries = parameterMap.entrySet();
        for (Map.Entry<String, String[]> entry : entries) {
            String key = entry.getKey();
            String paramName = HotParamRule.getParamName(rule);
            if (key.equals(paramName)) {
                isMatched = valueEqual(entry.getValue(), HotParamRule.getParamValue(rule), HotParamRule.getParamType(rule));
            }
        }
        return isMatched;
    }

    private static boolean valueEqual(String[] values, String paramValue, String paramType) {
        boolean eq = false;
        for (String value : values) {
            if (value.equals(paramValue)) {
                eq = true;
                break;
            }
        }
        return eq;
    }


}

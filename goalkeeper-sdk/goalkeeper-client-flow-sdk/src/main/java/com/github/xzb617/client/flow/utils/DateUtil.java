package com.github.xzb617.client.flow.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具类
 * @author xzb617
 */
public class DateUtil {

    /**
     * 获取当前时间戳
     * @return
     */
    public static long getCurrentTimestamp() {
        return System.currentTimeMillis();
    }

    /**
     * 当前时间是否有效
     * <p>
     *     规则： 当开始时间不为空，则大于开始时间, 当结束时间不为空则要小于结束时间，
     *          当两者时间不为空时，要求结束时间必须大于开始时间
     * </p>
     * @param startTime 开始时间
     * @param finishTime 结束时间
     * @return
     */
    public static boolean isEffectiveTime(Date startTime, Date finishTime) {
        // 未配置时间，默认为永久有效
        if (startTime==null && finishTime==null) {
            return true;
        }
        long currentTimestamp = getCurrentTimestamp();
        if (startTime == null) {
            return currentTimestamp < finishTime.getTime();
        }
        if (finishTime == null) {
            return currentTimestamp > startTime.getTime();
        }
        long startTimestamp = startTime.getTime();
        long finishTimestamp = finishTime.getTime();
        return currentTimestamp > startTimestamp && currentTimestamp < finishTimestamp;
    }

    public static Date toDate(String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return dateFormat.parse(strDate);
        } catch (ParseException e) {
            return null;
        }
    }

}

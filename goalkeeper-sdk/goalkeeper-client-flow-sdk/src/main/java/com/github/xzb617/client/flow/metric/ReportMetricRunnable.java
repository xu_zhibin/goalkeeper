package com.github.xzb617.client.flow.metric;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportMetricRunnable implements Runnable {

    private final static Logger LOGGER = LoggerFactory.getLogger(ReportMetricRunnable.class);

    private final MetricService metricService;

    public ReportMetricRunnable(MetricService metricService) {
        this.metricService = metricService;
    }

    @Override
    public void run() {
        LOGGER.info("Reporting flow metrics data to server.");
        try {
            this.metricService.reportMetric();
        } catch (Throwable e) {
            LOGGER.error("An error occurred when reporting flow metrics data to server, cause by {}", e.getMessage(), e);
        }
    }

}

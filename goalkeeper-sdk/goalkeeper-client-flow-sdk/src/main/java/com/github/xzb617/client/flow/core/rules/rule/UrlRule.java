package com.github.xzb617.client.flow.core.rules.rule;

import com.github.xzb617.client.flow.cache.CounterCache;
import com.github.xzb617.client.flow.constant.CacheKey;
import com.github.xzb617.client.flow.core.FlowContext;
import com.github.xzb617.client.flow.core.FlowException;
import com.github.xzb617.client.flow.core.FlowProperties;
import com.github.xzb617.client.flow.core.rules.chain.Rule;
import com.github.xzb617.client.flow.core.rules.chain.RuleChain;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class UrlRule implements Rule {

    @Override
    public void validateInternal(FlowContext context, FlowProperties.RulesProperties config, CounterCache cache, RuleChain chain) throws FlowException {
        // 获取ServiceId
        String uri = context.getUri();
        // 获取配置
        Map<String, Integer> limits = config.getLimits().get("URL");
        if (limits!=null && limits.containsKey(uri)) {
            long rate = limits.get(uri);
            // 从缓存中获取速率
            long urlRateFromCache = cache.incrementAndGet(getCacheKey(uri));
            if (urlRateFromCache > rate) {
                throw new FlowException("Too Many Request");
            }
        }
    }

    private String getCacheKey(String url) {
        return CacheKey.FLOW + ":rules:url:" + url;
    }
}

package com.github.xzb617.client.flow.adapte.boot;

import com.github.xzb617.client.flow.core.FlowException;
import com.github.xzb617.client.flow.core.FlowProperties;
import com.github.xzb617.client.flow.fallback.FlowFallback;
import com.github.xzb617.client.flow.fallback.FlowResponse;
import com.github.xzb617.client.flow.utils.ResponseUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BootFlowFallback implements FlowFallback {

    private final FlowProperties.RejectProperties rejectProperties;

    public BootFlowFallback(FlowProperties flowProperties) {
        this.rejectProperties = flowProperties.getReject();
    }

    @Override
    public FlowResponse fallback(FlowException e) {
        return new FlowResponse(this.rejectProperties.getResponseStatus(), this.rejectProperties.getResponseContent());
    }

}

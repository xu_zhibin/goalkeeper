package com.github.xzb617.client.flow.fallback;

public class FlowResponse {

    private int responseStatus;

    private String responseContent;

    private FlowResponse() {
    }

    public FlowResponse(int responseStatus, String responseContent) {
        this.responseStatus = responseStatus;
        this.responseContent = responseContent;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public String getResponseContent() {
        return responseContent;
    }
}

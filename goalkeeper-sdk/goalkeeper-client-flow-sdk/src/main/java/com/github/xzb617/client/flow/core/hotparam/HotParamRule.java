package com.github.xzb617.client.flow.core.hotparam;

import com.github.xzb617.client.flow.utils.DateUtil;

import java.util.Date;
import java.util.Map;

/**
 * 热点参数规则
 * @author xzb617
 */
public class HotParamRule {

    private final static String RULE_NAME = "rule-name";

    private final static String PARAM_NAME = "param-name";

    private final static String PARAM_TYPE = "param-type";

    private final static String PARAM_VALUE = "param-value";

    private final static String RATE = "rate";

    private final static String START_TIME = "start-time";

    private final static String FINISH_TIME = "finish-time";

    public static String getRuleName(Map<String, Object> rule) {
        Object ruleName = rule.get(RULE_NAME);
        return ruleName==null?null:String.valueOf(ruleName);
    }


    public static String getParamName(Map<String, Object> rule) {
        Object paramName = rule.get(PARAM_NAME);
        return paramName==null?null:String.valueOf(paramName);
    }


    public static String getParamType(Map<String, Object> rule) {
        Object paramType = rule.get(PARAM_TYPE);
        return paramType==null?null:String.valueOf(paramType);
    }


    public static String getParamValue(Map<String, Object> rule) {
        Object paramValue = rule.get(PARAM_VALUE);
        return paramValue==null?null:String.valueOf(paramValue);
    }


    public static Integer getRate(Map<String, Object> rule) {
        Object rate = rule.get(RATE);
        return rate==null?null:(int)rate;
    }

    public static Date getStartTime(Map<String, Object> rule) {
        Object startTime = rule.get(START_TIME);
        return startTime==null?null: DateUtil.toDate(String.valueOf(startTime));
    }


    public static Date getFinishTime(Map<String, Object> rule) {
        Object finishTime = rule.get(FINISH_TIME);
        return finishTime==null?null:DateUtil.toDate(String.valueOf(finishTime));
    }

}

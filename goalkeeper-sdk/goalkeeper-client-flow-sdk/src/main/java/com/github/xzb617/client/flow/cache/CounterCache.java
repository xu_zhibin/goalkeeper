package com.github.xzb617.client.flow.cache;

public interface CounterCache {

    public long incrementAndGet(String key);

}

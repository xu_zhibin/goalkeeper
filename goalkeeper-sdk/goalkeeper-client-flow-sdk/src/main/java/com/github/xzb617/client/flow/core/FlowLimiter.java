package com.github.xzb617.client.flow.core;

import com.github.xzb617.client.flow.cache.CounterCache;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 流量限制器
 * @author xzb617
 */
public interface FlowLimiter {

    default boolean enabled(FlowProperties flowConfig) { return false; }

    default void execute(FlowContext flowContext,FlowProperties flowConfig,
                         CounterCache counterCache, FlowLimiter limiterChain) throws FlowException {
        // 是否使用本规则
        if (this.enabled(flowConfig)) {
            this.executeInternal(flowContext, flowConfig, counterCache, limiterChain);
        }
        // 执行下一个限流模式
        limiterChain.execute(flowContext, flowConfig, counterCache, limiterChain);
    }

    public void executeInternal(FlowContext flowContext, FlowProperties flowConfig, CounterCache counterCache, FlowLimiter limiterChain) throws FlowException;

}

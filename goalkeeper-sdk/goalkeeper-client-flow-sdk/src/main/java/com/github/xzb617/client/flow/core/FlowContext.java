package com.github.xzb617.client.flow.core;

import java.util.List;
import java.util.Map;

public class FlowContext {

    private Long qps;

    private String ip;

    private String uri;

    private Map<String, String[]> queryParams;

    public FlowContext() {
    }

    public FlowContext(Long qps, String ip, String uri, Map<String, String[]> queryParams) {
        this.qps = qps;
        this.ip = ip;
        this.uri = uri;
        this.queryParams = queryParams;
    }


    public Long getQps() {
        return qps;
    }

    public void setQps(Long qps) {
        this.qps = qps;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Map<String, String[]> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(Map<String, String[]> queryParams) {
        this.queryParams = queryParams;
    }
}

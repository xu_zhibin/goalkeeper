package com.github.xzb617.client.flow.core;

import com.github.xzb617.client.flow.cache.CounterCache;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 流量限制器链
 * <p>
 *     由不同模式的限制器组成链条
 * </p>
 * @author xzb617
 */
public class FlowLimiterChain implements FlowLimiter {

    private final List<FlowLimiter> limiters;
    private final int size;
    private int position;

    public FlowLimiterChain(List<FlowLimiter> limiters) {
        this.limiters = limiters;
        this.size = limiters.size();
        this.position = 0;
    }

    @Override
    public boolean enabled(FlowProperties flowConfig) {
        return false;
    }

    @Override
    public void execute(FlowContext flowContext, FlowProperties flowConfig, CounterCache counterCache, FlowLimiter chain) throws FlowException {
        if (this.position != this.size) {
            // 调用过滤链中过滤器的执行方法
            ++ this.position;
            FlowLimiter nextLimiter = this.limiters.get(this.position - 1);
            nextLimiter.execute(flowContext, flowConfig, counterCache, this);
        }
    }

    @Override
    public void executeInternal(FlowContext flowContext, FlowProperties flowConfig, CounterCache counterCache, FlowLimiter limiterChain) throws FlowException {

    }
}

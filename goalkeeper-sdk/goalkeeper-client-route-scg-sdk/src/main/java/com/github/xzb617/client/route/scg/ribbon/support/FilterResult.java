package com.github.xzb617.client.route.scg.ribbon.support;


import com.netflix.loadbalancer.Server;

import java.util.ArrayList;
import java.util.List;

public class FilterResult {

    private List<Server> targetServers = new ArrayList<>();

    private List<Server> othersServers = new ArrayList<>();

    public FilterResult() {
    }

    public FilterResult(List<Server> targetServers, List<Server> othersServers) {
        this.targetServers = targetServers;
        this.othersServers = othersServers;
    }

    public List<Server> getTargetServers() {
        return targetServers;
    }

    public void setTargetServers(List<Server> targetServers) {
        this.targetServers = targetServers;
    }

    public List<Server> getOthersServers() {
        return othersServers;
    }

    public void setOthersServers(List<Server> othersServers) {
        this.othersServers = othersServers;
    }
}

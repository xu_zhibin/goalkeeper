package com.github.xzb617.client.route.scg.transfers;

import com.github.xzb617.client.route.scg.constant.TransferKey;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import java.util.*;

/**
 * 头参透传
 * @author xzb617
 */
public class ScgHeaderTransfer {

    /**
     * 匹配所有需要透传的请求头
     * @param headers 请求头
     * @return HttpHeaders
     */
    public static void matchNeedTransferHeadersAndAddToScgHeader(HttpHeaders headers, ServerHttpRequest request) {
        Set<Map.Entry<String, List<String>>> entries = request.getHeaders().entrySet();
        for (Map.Entry<String, List<String>> header : entries) {
            String headerName = header.getKey();
            if (headerName!=null && headerName.startsWith(TransferKey.HEADER_TRANSFER_PREFIX)) {
                List<String> headerValue = header.getValue();
                headers.addAll(headerName, headerValue);
            }
        }
    }

}

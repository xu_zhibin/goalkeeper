package com.github.xzb617.client.route.scg.rules.impl;

import com.github.xzb617.client.route.scg.constant.RouteRule;
import com.github.xzb617.client.route.scg.props.RouteProperties;
import com.github.xzb617.client.route.scg.rules.RoutingRule;
import com.github.xzb617.client.route.scg.rules.ratio.WeightCalculator;
import com.github.xzb617.client.route.scg.utils.RouteUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 支持Get类型的查询参数匹配规则
 * @author xzb617
 */
public class QueryParamRoutingRule implements RoutingRule {

    @Override
    public void matches(ServerHttpRequest request, HttpHeaders httpHeaders, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain) {
        Map<String, Map<String, RouteRule>> rules = config.getRules();
        Map<String, RouteRule> queryParamRuleMap = rules.get("QUERY");
        if (queryParamRuleMap!=null && !queryParamRuleMap.isEmpty()) {
            RouteRule matchedRouteRule = this.matchQueryParam(request, queryParamRuleMap);
            if (matchedRouteRule != null) {
                boolean available = WeightCalculator.available(ipAddr, matchedRouteRule.getWeight());
                if (available) {
                    // 匹配成功，打上标签
                    RouteUtil.assignRouteLabel(httpHeaders, matchedRouteRule, true);
                } else {
                    RouteUtil.assignRouteLabel(httpHeaders, matchedRouteRule, false);
                }
            }
        }
    }

    /**
     * 匹配参数的key和value
     * @param request
     * @param queryParamRuleMap
     * @return
     */
    private RouteRule matchQueryParam(ServerHttpRequest request, Map<String, RouteRule> queryParamRuleMap) {
        RouteRule matchedRouteRule = null;
        MultiValueMap<String, String> queryParams = request.getQueryParams();
        Set<Map.Entry<String, List<String>>> entries = queryParams.entrySet();
        for (Map.Entry<String, List<String>> entry : entries) {
            String paramName = entry.getKey();
            if (queryParamRuleMap.containsKey(paramName)) {
                RouteRule routeRule = queryParamRuleMap.get(paramName);
                String callerValue = routeRule.getCallerValue();
                String paramValue = queryParams.getFirst(paramName);
                if (callerValue!=null && callerValue.equals(paramValue)) {
                    matchedRouteRule = routeRule;
                    break;
                }
            }
        }
        return matchedRouteRule;
    }
}

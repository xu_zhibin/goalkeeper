package com.github.xzb617.client.route.scg.constant;

/**
 * 路由规则
 * @author xzb617
 */
public class RouteRule {

    /**
     * 调用方服务名称，* ：所有服务
     */
    private String callerAppId = "*";

    private String callerKey;

    private String callerValue;

    /**
     * 被调方服务名称，* ：所有服务
     */
    private String calleeAppId = "*";

    private String calleeLabel;

    private String calleeLabelValue;

    /**
     * 流量比重，0-100, 默认：100
     * 比重是按照ip地址进行hash权重分配
     */
    private Integer weight = 100;

    public String getCallerAppId() {
        return callerAppId;
    }

    public void setCallerAppId(String callerAppId) {
        this.callerAppId = callerAppId;
    }

    public String getCallerKey() {
        return callerKey;
    }

    public void setCallerKey(String callerKey) {
        this.callerKey = callerKey;
    }

    public String getCallerValue() {
        return callerValue;
    }

    public void setCallerValue(String callerValue) {
        this.callerValue = callerValue;
    }

    public String getCalleeAppId() {
        return calleeAppId;
    }

    public void setCalleeAppId(String calleeAppId) {
        this.calleeAppId = calleeAppId;
    }

    public String getCalleeLabel() {
        return calleeLabel;
    }

    public void setCalleeLabel(String calleeLabel) {
        this.calleeLabel = calleeLabel;
    }

    public String getCalleeLabelValue() {
        return calleeLabelValue;
    }

    public void setCalleeLabelValue(String calleeLabelValue) {
        this.calleeLabelValue = calleeLabelValue;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}

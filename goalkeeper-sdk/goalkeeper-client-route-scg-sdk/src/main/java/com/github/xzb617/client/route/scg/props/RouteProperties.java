package com.github.xzb617.client.route.scg.props;

import com.github.xzb617.client.route.scg.constant.RouteRule;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 路由模块的可配置属性
 * @author xzb617
 */
@RefreshScope
@Component
@ConfigurationProperties(prefix = "goalkeeper.route")
public class RouteProperties {

    /**
     * 是否启用网关路由模块功能
     */
    private Boolean enabled = true;

    private RuleProperties rule = new RuleProperties();

    /**
     * 自定义规则式路由
     *
     * A -> B -> C
     * ip:
     */
    public class RuleProperties {

        private Map<String, Map<String, RouteRule>> rules = new HashMap<>();

        public Map<String, Map<String, RouteRule>> getRules() {
            return rules;
        }

        public void setRules(Map<String, Map<String, RouteRule>> rules) {
            this.rules = rules;
        }

    }


    public RuleProperties getRule() {
        return rule;
    }

    public void setRule(RuleProperties rule) {
        this.rule = rule;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}

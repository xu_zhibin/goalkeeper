package com.github.xzb617.client.route.scg.utils;

import org.springframework.util.Assert;

/**
 * IP地址hash工具类
 * @author xzb617
 */
public class IpHashUtil {

    /**
     * 对IP地址进行hash取值
     * @param ip ip地址
     * @return
     */
    public static int hash(String ip) {
        Assert.notNull(ip, "IP cannot be empty when hash is calculated.");
        // 进行与位操作，因为hashcode()可能为负数
        return ip.hashCode() & Integer.MAX_VALUE;
    }

}

package com.github.xzb617.client.route.scg.rules;

import com.github.xzb617.client.route.scg.props.RouteProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;


public interface RoutingRule {

    default boolean enabled() { return true; }

    default void executeMatches(ServerHttpRequest request, HttpHeaders httpHeaders, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain) {
        // 是否使用本规则
        if (this.enabled()) {
            this.matches(request, httpHeaders, ipAddr, config, routingRuleChain);
        }
        // 执行下一个规则
        routingRuleChain.executeMatches(request, httpHeaders, ipAddr, config, routingRuleChain);
    }

    public void matches(ServerHttpRequest request, HttpHeaders httpHeaders, String ipAddr, RouteProperties.RuleProperties config, RoutingRule routingRuleChain);

}

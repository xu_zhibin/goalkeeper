package com.github.xzb617.client.route;

import com.github.xzb617.client.route.feign.GoalkeeperRouteFeignInterceptor;
import com.github.xzb617.client.route.ribbon.GoalkeeperRoundRobinRule;
import com.netflix.niws.loadbalancer.DiscoveryEnabledNIWSServerList;
import feign.RequestInterceptor;
import io.jmnarloch.spring.cloud.ribbon.rule.DiscoveryEnabledRule;
import io.jmnarloch.spring.cloud.ribbon.support.RibbonDiscoveryRuleAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ConditionalOnClass(DiscoveryEnabledNIWSServerList.class)
@AutoConfigureBefore({RibbonClientConfiguration.class, RibbonDiscoveryRuleAutoConfiguration.class})
public class GoalKeeperClientRouteSdkAutoConfiguration {

    @Bean
    public RequestInterceptor goalkeeperRouteFeignInterceptor() {
        return new GoalkeeperRouteFeignInterceptor();
    }

    @Bean
    @Scope("prototype")
    public DiscoveryEnabledRule goalkeeperMetadataBalancerRule() {
        return new GoalkeeperRoundRobinRule();
    }

}

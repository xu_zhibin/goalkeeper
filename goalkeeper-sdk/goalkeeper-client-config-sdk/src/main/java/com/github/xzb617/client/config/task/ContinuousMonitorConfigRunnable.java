package com.github.xzb617.client.config.task;

import com.github.xzb617.client.config.refresher.ConfigurableContextRefresher;
import com.github.xzb617.client.config.serv.ConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 长轮询监听配置变化状态
 * <p>1.服务端超时时间为 60 秒</p>
 * @author xzb617
 */
public class ContinuousMonitorConfigRunnable implements Runnable {

    private final static Logger LOGGER = LoggerFactory.getLogger(ContinuousMonitorConfigRunnable.class);
    private final ConfigService configService;
    private final ConfigurableContextRefresher configurableContextRefresher;

    public ContinuousMonitorConfigRunnable(ConfigService configService, ConfigurableContextRefresher configurableContextRefresher) {
        this.configService = configService;
        this.configurableContextRefresher = configurableContextRefresher;
    }

    @Override
    public void run() {
        try {
            boolean modified = this.configService.monitorConfig();
            if (modified) {
                // 有配置文件更新
                this.configurableContextRefresher.refreshEnvironment();
            } else {
                LOGGER.info("No remote configuration can be refreshed.");
            }
        } catch (Throwable e) {
            LOGGER.error("An error occurred while executing the scheduled pull configuration, cause by {}", e.getMessage(), e);
        }
    }

}

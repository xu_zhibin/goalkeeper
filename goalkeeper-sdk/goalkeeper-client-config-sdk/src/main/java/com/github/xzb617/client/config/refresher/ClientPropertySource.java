package com.github.xzb617.client.config.refresher;

import org.springframework.core.env.MapPropertySource;

import java.util.Map;

public class ClientPropertySource extends MapPropertySource {

    public ClientPropertySource(Map<String, Object> source) {
        super("clientPropertySource", source);
    }

}

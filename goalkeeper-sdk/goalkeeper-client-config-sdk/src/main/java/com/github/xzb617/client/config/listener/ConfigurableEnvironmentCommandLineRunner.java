package com.github.xzb617.client.config.listener;

import com.github.xzb617.client.base.props.BaseProperties;
import com.github.xzb617.client.base.props.ClientProperties;
import com.github.xzb617.client.config.props.ClientConfigProperties;
import com.github.xzb617.client.config.refresher.ConfigurableContextRefresher;
import com.github.xzb617.client.config.serv.ConfigService;
import com.github.xzb617.client.config.serv.ConfigServiceFactory;
import com.github.xzb617.client.config.task.ContinuousMonitorConfigRunnable;
import com.github.xzb617.client.config.task.FixedTimeGetConfigRunnable;
import com.github.xzb617.common.executors.GoalKeeperExecutors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ConfigurableEnvironmentCommandLineRunner implements CommandLineRunner {

    private final static Logger LOGGER = LoggerFactory.getLogger(ConfigurableEnvironmentCommandLineRunner.class);
    private final ClientProperties clientProperties;
    private final ClientConfigProperties configProperties;
    private final BaseProperties baseProperties;
    private final ConfigurableContextRefresher configurableContextRefresher;

    private final ScheduledExecutorService monitorConfigService = GoalKeeperExecutors.newSingleThreadScheduledExecutor("monitor-config", true);
    private final ScheduledExecutorService fixedGetConfigService = GoalKeeperExecutors.newSingleThreadScheduledExecutor("fixed-get-config", true);

    public ConfigurableEnvironmentCommandLineRunner(ClientProperties clientProperties, ClientConfigProperties configProperties, BaseProperties baseProperties, ConfigurableContextRefresher configurableContextRefresher) {
        this.clientProperties = clientProperties;
        this.configProperties = configProperties;
        this.baseProperties = baseProperties;
        this.configurableContextRefresher = configurableContextRefresher;
    }

    @Override
    public void run(String... args) throws Exception {
        Boolean refreshEnabled = this.configProperties.getRefreshEnabled();
        if (refreshEnabled) {
            LOGGER.info("The auto refresh configuration from config server is enabled on the current client.");
            // 获取 ConfigService
            ConfigService configService = ConfigServiceFactory.getInstance(this.clientProperties, this.baseProperties, this.configProperties);

            // 监听配置在服务端的变化，一有变化就能感知
            ContinuousMonitorConfigRunnable monitorConfigRunnable = new ContinuousMonitorConfigRunnable(configService, this.configurableContextRefresher);
            this.monitorConfigService.scheduleAtFixedRate(monitorConfigRunnable, 2000,  1000, TimeUnit.MILLISECONDS);

            // 定期获取配置校对
            FixedTimeGetConfigRunnable fixedGetConfigRunnable = new FixedTimeGetConfigRunnable(configService, this.configurableContextRefresher);
            this.fixedGetConfigService.scheduleAtFixedRate(fixedGetConfigRunnable, 30000, 30000, TimeUnit.MILLISECONDS);
        }
    }

}

package com.github.xzb617.client.config.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = ClientConfigProperties.PREFIX)
public class ClientConfigProperties {

    public final static String PREFIX = "goalkeeper.config";

    /**
     * 配置分组，默认： DEFAULT_GROUP
     */
    private String group = "DEFAULT_GROUP";

    /**
     * 配置文件在服务端的名称, 默认：DEFAULT_NAME
     */
    private String name = "DEFAULT_NAME";

    /**
     * 配置文件类型， 默认：yaml
     */
    private String fileExtension = "yaml";

    /**
     * 是否开启自动刷新，不开启的情况下只会在项目启动时加载一次配置中心的配置，开启时会额外占用一定的资源
     */
    private Boolean refreshEnabled = true;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public Boolean getRefreshEnabled() {
        return refreshEnabled;
    }

    public void setRefreshEnabled(Boolean refreshEnabled) {
        this.refreshEnabled = refreshEnabled;
    }

}

package com.github.xzb617.client.config.refresher;

import org.springframework.cloud.context.refresh.ContextRefresher;

/**
 * 配置刷新逻辑
 */
public class ConfigurableContextRefresher {

    private final ContextRefresher contextRefresher;

    public ConfigurableContextRefresher(ContextRefresher contextRefresher) {
        this.contextRefresher = contextRefresher;
    }

    public void refreshEnvironment() {
        // 重置标记缓存
        RefresherCache.resetCache();
        // 刷新配置
        this.contextRefresher.refresh();
    }
}

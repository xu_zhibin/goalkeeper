package com.github.xzb617.client.config.serv;

import com.github.xzb617.client.base.props.ClientProperties;
import com.github.xzb617.client.base.props.BaseProperties;
import com.github.xzb617.client.config.props.ClientConfigProperties;
import com.github.xzb617.rpc.LoadBalanceCaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ConfigService {

    private final static Logger LOGGER = LoggerFactory.getLogger(ConfigService.class);

    private final ClientProperties clientProperties;
    private final BaseProperties baseProperties;
    private final ClientConfigProperties configProperties;
    private final LoadBalanceCaller loadBalanceCaller;

    public ConfigService(ClientProperties clientProperties, BaseProperties baseProperties, ClientConfigProperties configProperties, LoadBalanceCaller loadBalanceCaller) {
        this.clientProperties = clientProperties;
        this.baseProperties = baseProperties;
        this.configProperties = configProperties;
        this.loadBalanceCaller = loadBalanceCaller;
    }

    public Map<String, Object> getConfigFromServer() {
        ResponseEntity<Map> rpcResponse = this.loadBalanceCaller.lbGet("/clients/getConfig", params(), headers(), Map.class);
        if (rpcResponse.getStatusCode() == HttpStatus.NO_CONTENT) {
            // 没有对应的配置（客户端不存在 / 配置不存在）
            return null;
        }
        return rpcResponse.getBody();
    }


    public Config getConfigStatus() {
        ResponseEntity<Config> rpcResponse = this.loadBalanceCaller.lbGet("/clients/getConfigStatus", params(), headers(), Config.class);
        if (rpcResponse.getStatusCode() == HttpStatus.NO_CONTENT) {
            // 没有对应的配置（客户端不存在 / 配置不存在）
            return null;
        }
        return rpcResponse.getBody();
    }

    public boolean monitorConfig() {
        ResponseEntity<Boolean> rpcResponse = this.loadBalanceCaller.lbGet("/clients/monitorConfig", params(), headers(), Boolean.class);
        return rpcResponse.getStatusCode() == HttpStatus.OK;
    }

    public void shutdownClient() {
//        this.loadBalanceCaller.lbGet(ClientApi.SHUTDOWN, this.meta.toParameterMap());
    }

    private Map<String, String> headers() {
        Map<String, String> headers = new HashMap<>(4);
        headers.put("instance-id", this.clientProperties.getInstanceId());
        headers.put("access-key", this.clientProperties.getAccessKey());
        headers.put("access-secret", this.clientProperties.getAccessSecret());
        return headers;
    }

    private Map<String, String> params() {
        Map<String, String> params = new HashMap<>(2);
        params.put("config_group", this.configProperties.getGroup());
        params.put("config_name", this.configProperties.getName());
        params.put("flow_name", this.baseProperties.getFlow().getName());
        params.put("route_name", this.baseProperties.getRoute().getName());
        return params;
    }

}

package com.github.xzb617.client.config;

import com.github.xzb617.client.base.props.ClientProperties;
import com.github.xzb617.client.base.props.BaseProperties;
import com.github.xzb617.client.config.listener.ConfigurableEnvironmentCommandLineRunner;
import com.github.xzb617.client.config.props.ClientConfigProperties;
import com.github.xzb617.client.config.refresher.ConfigurableContextRefresher;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置中心自动装配
 * @author xzb617
 */
@Configuration
@EnableConfigurationProperties({ClientProperties.class, ClientConfigProperties.class})
public class GoalKeeperClientConfigSdkAutoConfiguration {

    @Bean
    public ConfigurableContextRefresher configurableContextRefresher(ContextRefresher contextRefresher) {
        return new ConfigurableContextRefresher(contextRefresher);
    }

    @Bean
    public ConfigurableEnvironmentCommandLineRunner configurableEnvironmentCommandLineRunner(ClientProperties clientProperties, BaseProperties baseProperties, ClientConfigProperties clientConfigProperties, ConfigurableContextRefresher configurableContextRefresher) {
        return new ConfigurableEnvironmentCommandLineRunner(clientProperties, clientConfigProperties, baseProperties, configurableContextRefresher);
    }
}

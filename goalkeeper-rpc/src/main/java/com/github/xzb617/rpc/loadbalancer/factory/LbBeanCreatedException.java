package com.github.xzb617.rpc.loadbalancer.factory;

/**
 * Bean初始化失败
 */
public class LbBeanCreatedException extends Exception {

    public LbBeanCreatedException(String message) {
        super(message);
    }

    public LbBeanCreatedException(String message, Throwable cause) {
        super(message, cause);
    }

}

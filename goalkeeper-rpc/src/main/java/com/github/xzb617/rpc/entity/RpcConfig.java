package com.github.xzb617.rpc.entity;

import java.util.HashMap;
import java.util.Map;

public class RpcConfig {

    private Map<String, String> headers = new HashMap<String, String>();

    private Integer timeout = 10000;

    private String encode = "UTF-8";

    public RpcConfig() {
    }

    public RpcConfig(Map<String, String> headers, Integer timeout, String encode) {
        this.headers = headers;
        this.timeout = timeout;
        this.encode = encode;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getEncode() {
        return encode;
    }

    public void setEncode(String encode) {
        this.encode = encode;
    }
}

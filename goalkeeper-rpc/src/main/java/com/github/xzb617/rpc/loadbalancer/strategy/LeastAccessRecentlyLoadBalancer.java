package com.github.xzb617.rpc.loadbalancer.strategy;

import com.github.xzb617.rpc.entity.Server;
import com.github.xzb617.rpc.loadbalancer.AbstractLoadBalancer;

import java.util.List;

/**
 * 最近最少访问策略
 */
public class LeastAccessRecentlyLoadBalancer extends AbstractLoadBalancer {

    public Server chooseServer(List<Server> servers) {
        return this.acquireLeastAccessRecentlyServer(servers);
    }

    private Server acquireLeastAccessRecentlyServer(List<Server> servers) {
        int temp = 0;
        Server targetServer = null;
        for (Server server : servers) {
            int connectCounter = server.getConnectCounter().get();
            if (temp > connectCounter) {
                temp = connectCounter;
                targetServer = server;
            }
        }
        return targetServer;
    }
}

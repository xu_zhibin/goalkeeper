package com.github.xzb617.rpc.loadbalancer;

import com.github.xzb617.rpc.entity.Server;

import java.util.List;

public abstract class AbstractLoadBalancer implements LoadBalancer {

    /**
     * 如果最终结果返回的null，则要求必须抛出指定异常
     * @param servers 服务列表
     * @return
     */
    public Server choose(List<Server> servers) {
        if (servers==null || servers.size()==0) {
            return null;
        }
        if (servers.size() == 1) {
            return servers.get(0);
        }
        return this.chooseServer(servers);
    }

    /**
     * 选择服务
     * @param servers 服务列表
     * @return
     */
    public abstract Server chooseServer(List<Server> servers);

}

package com.github.xzb617.rpc.entity;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 服务
 */
public class Server {

    private String address;

    private Integer weight;

    private AtomicInteger connectCounter;

    public Server() {
        this.weight = 1;
        this.connectCounter = new AtomicInteger(0);
    }

    public Server(String address) {
        this.address = address;
        this.weight = 1;
        this.connectCounter = new AtomicInteger(0);
    }

    public Server(String address, Integer weight) {
        this.address = address;
        this.weight = weight;
        this.connectCounter = new AtomicInteger(0);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public AtomicInteger getConnectCounter() {
        return connectCounter;
    }

    public void setConnectCounter(AtomicInteger connectCounter) {
        this.connectCounter = connectCounter;
    }
}

package com.github.xzb617.rpc.core;

import com.github.xzb617.rpc.entity.Server;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface RequestCaller {

    public <T> ResponseEntity<T> get(Server server, String methodPath, Map<String, String> parameters, Map<String, String> headers, Class<T> responseType);

    public <T> ResponseEntity<T> post(Server server, String methodPath, Map<String, String> parameters, Map<String, String> headers, Class<T> responseType);

    public <T> ResponseEntity<T> put(Server server, String methodPath, Map<String, String> parameters, Map<String, String> headers, Class<T> responseType);

    public <T> ResponseEntity<T> delete(Server server, String methodPath, Map<String, String> parameters, Map<String, String> headers, Class<T> responseType);

}

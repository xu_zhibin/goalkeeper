package com.github.xzb617.rpc.loadbalancer;

import com.github.xzb617.rpc.entity.Server;

import java.util.List;

/**
 * 负载均衡器
 */
public interface LoadBalancer {

    Server choose(List<Server> servers);

}
